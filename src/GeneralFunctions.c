#include "orx.h"

#include "GeneralFunctions.h"

void DeleteAllBullets()
{
	// This goes through the linked-list of all objects that have been loaded, and compares
	//   their name with whatever string we pass in, returning the first match,
	//   or a NULL if no match is found.
	orxOBJECT* CurrentObject;
	orxOBJECT* NextObject;
	const orxSTRING TempName;

	CurrentObject = orxOBJECT( orxStructure_GetFirst( orxSTRUCTURE_ID_OBJECT ) );
	NextObject = CurrentObject;

	/* Loop through all the objects on the screen, deleting all bullets */
	do
	{
		CurrentObject = NextObject;
		NextObject = orxOBJECT( orxStructure_GetNext( NextObject ) );

		// Get the current object's name,
		TempName = orxObject_GetName(CurrentObject);
		// Compare that to our input string (objectName),
		if (!strcmp(TempName, "Circle"))
			orxObject_Delete(CurrentObject);
	} while (NextObject != orxNULL);

	return;
}

/*
 * Given an object, delete ALL the sibling objects, including itself
 *
 * any_sibling: A pointer to any of the siblings. The entire group of
 * siblings will be deleted.
 *
 */
void DeleteAllSiblings(orxOBJECT *any_sibling)
{
	orxOBJECT *next_sibling;
	orxOBJECT *current_sibling;

	next_sibling = any_sibling;

	do
	{
		current_sibling = next_sibling;
		next_sibling = orxObject_GetSibling(next_sibling);
		orxLOG("pointer next_sibling is %s", orxObject_GetName(next_sibling));
		orxLOG("pointer next_sibling is %s",
				orxObject_GetName(current_sibling));

		orxObject_Delete(current_sibling);
	} while (next_sibling != orxNULL);

	return;
}

/* Delete all enemies and bullets of the screen
 * different types of bombing will be implemented
 *
 */
/*void Bombing()
{
	orxOBJECT* CurrentObject;
	orxOBJECT* NextObject;
	const orxSTRING TempName;

	CurrentObject = orxOBJECT( orxStructure_GetFirst( orxSTRUCTURE_ID_OBJECT ) );
	NextObject = CurrentObject;

	/* Loop through all the objects on the screen, deleting all bullets
	do
	{
		CurrentObject = NextObject;
		NextObject = orxOBJECT( orxStructure_GetNext( NextObject ) );

		// Get the current object's name,
		TempName = orxObject_GetName(CurrentObject);
		// Compare that to our input string (objectName),
		if (!strcmp(TempName, "Circle") || !strcmp(TempName, "Square"))
			orxObject_Delete(CurrentObject);
	} while (NextObject != orxNULL);

	return;

}
*/
