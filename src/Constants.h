#include "orx.h"

#define TOP_LEFT 0
#define TOP_RIGHT 1
#define BOTTOM_LEFT 2
#define BOTTOM_RIGHT 3

/*
 Get a significant screen position in the game
 
 orx_vector: The ORX vector to place at the specified location
 screen_position: Where to place the vector
 */
void GetScreenPosition(orxVECTOR *orx_vector, int screen_position)
{

	switch (screen_position)
	{
	case TOP_LEFT:
		break;
	case TOP_RIGHT:
		break;
	case BOTTOM_LEFT:
		orxVector_Set(orx_vector, 512.f, 700.f, 0);
		break;
	case BOTTOM_RIGHT:
	default:
		break;
	}
}
;

