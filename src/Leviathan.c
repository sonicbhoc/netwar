/*
 * Leviathan.c
 *
 *  Created on: Nov 26, 2011
 *      Author: Levaithan
 */

#include "orx.h"
#include "Constants.h"
#include "GeneralFunctions.h"
#include <math.h>

/* TODO: Split into multiple "encapsulated" modules. */
/* This will be done to ease development, especially when the project starts to become larger. */

/* Symbolic Constants. */
#define NUM_MENUS        2
#define MAX_MENU_OPTIONS 6
#define MAX_MENU_ENTRIES 5

/* Global Variables. */
orxS32    menu_index       = 0,/* The index of the currently selected menu option. */
          option_index     = 0,
          entry_index      = 0,
          ship_id          = 0,
          num_lives        = 0,
          num_bombs        = 0,
          stage_id         = 0,
          score            = 0;
orxOBJECT *scene           = orxNULL, /* The parent of all other objects. */
		  *stage           = orxNULL,
		  *menus[NUM_MENUS][MAX_MENU_OPTIONS + 1][MAX_MENU_ENTRIES + 1], /* The array that contains pointers to all menu options in the current menu screen. */
          *description     = orxNULL, /* The description that is associated with the currently selected menu option. */
          *player          = orxNULL, /* Pointer to the player object. */
          *camera_parent   = orxNULL,
          *hud_pointer     = orxNULL,
          *BombingFlash    = orxNULL,
          *score_label     = orxNULL,
          *lives_label = orxNULL,
          *bombs_label = orxNULL,
          *boss_name_label = orxNULL;
orxVIEWPORT *viewport      = orxNULL; /* The viewport; what the player sees. */
orxBOOL new_scene          = orxFALSE;
orxSPAWNER *main_weapon;

orxOBJECT *bossPatterns[4];
orxOBJECT *bossHealthBars[4];
orxVECTOR scale;

/* Function Pointer to the types of shots */
void orxFASTCALL (*shoot_type)(orxSPAWNER *spawner, orxOBJECT *shot);
/* Red ship gun spread  */
static void orxFASTCALL laser_spread(orxSPAWNER *spawner, orxOBJECT *shot);
/* Blue ship gun spread */
static void orxFASTCALL homing_spread(orxSPAWNER *spawner, orxOBJECT *shot);
/* Green ship gun spread */
static void orxFASTCALL wide_spread(orxSPAWNER *spawner, orxOBJECT *shot);

orxSTATUS orxFASTCALL StageOneHandler(const orxEVENT *_pstEvent);
orxSTATUS orxFASTCALL StageTwoHandler(const orxEVENT *_pstEvent);
orxSTATUS orxFASTCALL StageThreeHandler(const orxEVENT *_pstEvent);

void orxFASTCALL UpdateHomingTrajectory(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext);
static void orxFASTCALL deleteBullets();
static void orxFASTCALL deleteEnemies(orxBOOL damage);

// enemys link list
orxLINKLIST enemyList;
orxLINKLIST bulletList;
//int NumBombsLeft = 1;

typedef struct
{
	orxLINKLIST_NODE llNode;
	orxOBJECT        *enemy;
	orxLINKLIST		 *shotlist;
	orxS32           health;
	orxS32           bars;
} orxENEMY;

typedef struct
{
	orxLINKLIST_NODE llNode;
	orxOBJECT        *shot;

} orxSHOT;

typedef struct
{
	orxLINKLIST_NODE llNode;
	orxOBJECT        *bullet;

} orxBULLET;

// ALLOCATE MEMORY FOR ALL LINKLIST'S NODE's
orxBANK *memBankLinkedList;
orxBANK *memBankEnemy;
orxBANK *memBankShot;
orxBANK *memBankBullet;

/* Function prototypes */
/* TODO: Prototype all functions, and stick them in a nice header file somewhere */
static orxINLINE orxSTATUS HighlightMenuOption(orxS32 new_index);
void orxFASTCALL SwitchMenu(orxS32 id);
void orxFASTCALL enableBossPattern(orxS32 bossPatternIndex, orxBOOL enable);
void orxFASTCALL addShotToLinkList(orxOBJECT *shot);

void orxFASTCALL enableBossPattern(orxS32 bossPatternIndex, orxBOOL enable)
{
	orxOBJECT *pattern = orxObject_GetChild(bossPatterns[bossPatternIndex]);
	do
		orxSpawner_Enable(orxOBJECT_GET_STRUCTURE(pattern, SPAWNER), enable);
	while ((pattern = orxObject_GetSibling(pattern)) != orxNULL);
}

void orxFASTCALL LoadMenu()
{
	for(menu_index = 0; menu_index < NUM_MENUS; menu_index++)
	{
		menus[menu_index][0][0] = (!menu_index) ? orxObject_GetChild(orxObject_GetChild(scene)) : orxObject_GetSibling(menus[menu_index - 1][0][0]);

		orxConfig_PushSection(orxObject_GetName(menus[menu_index][0][0]));
		for(option_index = 1; option_index <= orxConfig_GetListCounter("ChildList"); option_index++)
		{
			menus[menu_index][option_index][0] = (!(option_index - 1)) ? orxObject_GetChild(menus[menu_index][0][0]) : orxObject_GetSibling(menus[menu_index][option_index - 1][0]);

			if(orxObject_GetChild(menus[menu_index][option_index][0]))
			{
				orxConfig_PushSection(orxObject_GetName(menus[menu_index][option_index][0]));
				for(entry_index = 1; entry_index <= orxConfig_GetListCounter("ChildList"); entry_index++)
					menus[menu_index][option_index][entry_index] = (!(entry_index - 1)) ? orxObject_GetChild(menus[menu_index][option_index][0]) : orxObject_GetSibling(menus[menu_index][option_index][entry_index - 1]);
				orxConfig_PopSection();
			}
		}
		orxConfig_PopSection();
	}

	menu_index   = 0;
	option_index = 1;
	entry_index  = 0;

	description = orxObject_GetChild(orxObject_GetSibling(orxObject_GetChild(scene)));

	if (description)
	{
		/* Set the description text to the description associated with the selected menu option. */
		orxConfig_PushSection(orxObject_GetName(menus[menu_index][0][0]));
		orxObject_SetTextString(description,
				orxConfig_GetListString("DescriptionList", option_index - 1));
		orxConfig_PopSection();
	}

	SwitchMenu(0);

	return;
}

void orxFASTCALL SwitchMenu(orxS32 id)
{
	orxS32 option_traverse,
	       entry_traverse;
	/* TODO: After the event handler is fixed, move the input changes to the start and end of the menu animations. */
	/* Temporarily disable user input */
	orxInput_SelectSet("Discard");

	/* Unhilight current menu option */
	orxObject_RemoveFX(menus[menu_index][option_index][0], "HighlightFX");
	orxObject_AddFX(menus[menu_index][option_index][0], "UnhighlightFX");

	/* Slide out selected menu */
	if(id != menu_index)
	{
		orxConfig_PushSection(orxObject_GetName(menus[menu_index][0][0]));
		for(option_traverse = 1; option_traverse <= orxConfig_GetListCounter("ChildList"); option_traverse++)
		{
			orxObject_RemoveFX(menus[menu_index][option_index][0], "UnhighlightFX");
			orxObject_RemoveFX(menus[menu_index][option_traverse][0], "MenuFadeInFX");

			orxObject_AddDelayedFX(menus[menu_index][option_traverse][0], "MenuFadeOutFX", (orxFLOAT)(option_traverse - 1) * 0.05);
			orxObject_AddDelayedFX(menus[menu_index][option_traverse][0], "MenuSlideOutFX", (orxFLOAT)(option_traverse - 1) * 0.05);

			if(menus[menu_index][option_traverse][1])
			{
				orxConfig_PushSection(orxObject_GetName(menus[menu_index][option_traverse][0]));
				for(entry_traverse = 1; entry_traverse <= orxConfig_GetListCounter("ChildList"); entry_traverse++)
				{
					orxVECTOR position;
					orxObject_AddDelayedFX(menus[menu_index][option_traverse][entry_traverse], "MenuFadeOutFX", (orxFLOAT)(option_traverse - 1) * 0.05);
					orxConfig_PushSection(orxObject_GetName(menus[menu_index][option_traverse][entry_traverse]));
					orxObject_SetPosition(menus[menu_index][option_traverse][entry_traverse], orxConfig_GetVector("Position", &position));
					orxConfig_PopSection();
				}

				orxConfig_PopSection();
			}
		}
		orxConfig_PopSection();
	}

	/* Slide in new menu */
	menu_index = id;
	orxConfig_PushSection(orxObject_GetName(menus[menu_index][0][0]));
	for(option_traverse = 1; option_traverse <= orxConfig_GetListCounter("ChildList"); option_traverse++)
	{
		orxObject_RemoveFX(menus[menu_index][option_traverse][0], "MenuFadeOutFX");

		orxObject_AddDelayedFX(menus[menu_index][option_traverse][0], "MenuFadeInFX", (orxFLOAT)(option_traverse - 1) * 0.05);
		orxObject_AddDelayedFX(menus[menu_index][option_traverse][0], "MenuSlideInFX", (orxFLOAT)(option_traverse - 1) * 0.05);

		if(menus[menu_index][option_traverse][1])
				orxObject_AddDelayedFX(menus[menu_index][option_traverse][1], "MenuFadeInFX", (orxFLOAT)(option_traverse - 1) * 0.05);

	}
	orxConfig_PopSection();

	/* Select that new menu's first option */
	option_index = 1;
	HighlightMenuOption(1);

	/* Restore user input */
	orxInput_SelectSet("MenuControls");

	return;
}


void orxFASTCALL StartEnemyWave(const orxCLOCK_INFO *_pstClockInfo, void *_pContext)
{
	static orxS32 CurrentWave = 0;
	orxConfig_PushSection("Choreography");
	orxObject_CreateFromConfig(orxConfig_GetListString("EnemyWaveList", CurrentWave++));
	orxConfig_PopSection();
	return;
}

/* This code loads the game. */
static orxSTATUS orxFASTCALL LoadGame()
{
	orxCLOCK *waveClock;
	const orxCLOCK_INFO *main_clock_info;
	orxSTATUS result = orxSTATUS_FAILURE;
	orxS32 delay_index = 0;
	orxFLOAT total_wave_delay = 0;
	int i;


	/* Load the selected character */
	orxConfig_PushSection("CharacterList");
	orxConfig_Load(orxConfig_GetListString("ConfigList", ship_id));
	orxConfig_PopSection();

	/* Load the stages to play. */
	/* An Idea: Create a linked list in code to hold the stage sequence; */
	/* The trailer node should just be code to load a screen */
	/* that allows entry into the high score table and maybe shows some stats (how far you made it, how many bombs you used, etc.) */
	orxConfig_PushSection("StageSequence");
	orxConfig_Load(orxConfig_GetListString("ConfigList", stage_id));
	orxConfig_PopSection();

	/* Create the viewport */
	viewport = orxViewport_CreateFromConfig("Viewport");

	/* Create the scene */
	scene = orxObject_CreateFromConfig("Scene");

	orxLOG("Scene Child Sibling name: %s", orxObject_GetName(orxObject_GetSibling(orxObject_GetSibling(orxObject_GetSibling(orxObject_GetSibling(orxObject_GetChild(orxObject_GetSibling(orxObject_GetChild(scene)))))))));

	/* Get Boming Flash */
	BombingFlash = orxObject_GetSibling(orxObject_GetSibling(orxObject_GetSibling(orxObject_GetSibling(orxObject_GetChild(orxObject_GetSibling(orxObject_GetChild(scene)))))));

	/* Get player object */
	orxLOG("Player points to %s", orxObject_GetName(orxObject_GetChild(scene)));
	player = orxObject_GetChild(scene);

	/* Create the camera's parent object */
	camera_parent = orxObject_CreateFromConfig("CameraParent");
	orxCamera_SetParent(orxViewport_GetCamera(viewport), camera_parent);

	/* Get pointer to the Hud */
	hud_pointer = orxObject_GetSibling(orxObject_GetChild(scene));

	/* Get spawner pointer */
	main_weapon = orxOBJECT_GET_STRUCTURE(player, SPAWNER);

	/* Depending on the main weapon, change the function pointer to point to the correct bullet manipulation function */
	if(!orxString_Compare(orxSpawner_GetName(main_weapon), "RedSpawner"))
		shoot_type = &laser_spread;
	else if(!orxString_Compare(orxSpawner_GetName(main_weapon), "BlueSpawner"))
		shoot_type = &homing_spread;
	else
		shoot_type = &wide_spread;

	/* Get pointer to orxSTRING representing the number of lives. */

	/* Get pointer to orxSTRING representing the number of bombs. */

	/* Get pointer to orxSTRING representing the score. */

	/* Set game controls */
	orxInput_SelectSet("UserGameControls");

	stage = orxObject_GetSibling(orxObject_GetSibling(orxObject_GetChild(scene)));

	switch(stage_id)
	{
	case 0:
		orxEvent_AddHandler(orxEVENT_TYPE_FX, StageOneHandler);
		orxEvent_AddHandler(orxEVENT_TYPE_SPAWNER, StageOneHandler);
		orxEvent_AddHandler(orxEVENT_TYPE_OBJECT, StageOneHandler);
		break;

	case 1:
		orxEvent_AddHandler(orxEVENT_TYPE_FX, StageTwoHandler);
		orxEvent_AddHandler(orxEVENT_TYPE_SPAWNER, StageTwoHandler);
		orxEvent_AddHandler(orxEVENT_TYPE_OBJECT, StageTwoHandler);
		break;

	case 2:
		orxEvent_AddHandler(orxEVENT_TYPE_FX, StageThreeHandler);
		orxEvent_AddHandler(orxEVENT_TYPE_SPAWNER, StageThreeHandler);
		orxEvent_AddHandler(orxEVENT_TYPE_OBJECT, StageThreeHandler);
		break;
	}

	/* Create clock to control enemy wave patterns */
	main_clock_info = orxClock_GetInfo(orxClock_FindFirst(orx2F(-1.0f), orxCLOCK_TYPE_CORE));
	waveClock = orxClock_CreateFromConfig("WaveClock");
	orxClock_SetModifier(waveClock, main_clock_info->eModType, main_clock_info->fModValue);
	orxClock_Restart(waveClock);

	/* Add timer to the clock */
	if(orxConfig_PushSection("Choreography"))
	{
		for (delay_index = 0; delay_index < orxConfig_GetListCounter("ListDelay"); delay_index++)
		{
			total_wave_delay += orxConfig_GetListFloat("ListDelay", delay_index);
			orxClock_AddTimer(waveClock, StartEnemyWave, total_wave_delay, 1, orxNULL);
		}
		orxConfig_PopSection();
	}

	/*Registering our homing update function */
	orxClock_Register(orxClock_CreateFromConfig("HomingClock"), UpdateHomingTrajectory, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);
	orxClock_SetModifier(orxClock_Get("HomingClock"), main_clock_info->eModType, main_clock_info->fModValue);
	orxClock_Restart(orxClock_Get("HomingClock"));

	boss_name_label =
		orxObject_GetSibling(orxObject_GetChild(
			orxObject_GetSibling(orxObject_GetSibling(
				orxObject_GetChild(orxObject_GetSibling(orxObject_GetChild(scene)))))));
	bossHealthBars[0] =
		orxObject_GetChild(orxObject_GetChild(
			orxObject_GetSibling(orxObject_GetSibling(
				orxObject_GetChild(orxObject_GetSibling(orxObject_GetChild(scene)))))));
	for (i = 1; i < 4; i++)
		bossHealthBars[i] = orxObject_GetSibling(bossHealthBars[i-1]);

	scale.fY = 16;
	scale.fZ = 0;

	score_label =
		orxObject_GetSibling(orxObject_GetChild(
			orxObject_GetSibling(orxObject_GetChild(
				orxObject_GetSibling(orxObject_GetChild(
					orxObject_GetSibling(orxObject_GetChild(scene))))))));

	lives_label =
		orxObject_GetSibling(orxObject_GetChild(
			orxObject_GetSibling(orxObject_GetSibling(orxObject_GetChild(
				orxObject_GetSibling(orxObject_GetChild(
					orxObject_GetSibling(orxObject_GetChild(scene)))))))));

	num_lives = 9 - (num_lives * 2);

	char LivesString[4];
	sprintf(LivesString, "%d", (int)num_lives);
	orxObject_SetTextString(lives_label, LivesString);

	bombs_label =
			orxObject_GetSibling(orxObject_GetChild(
			orxObject_GetSibling(orxObject_GetSibling(orxObject_GetSibling(orxObject_GetChild(
				orxObject_GetSibling(orxObject_GetChild(
					orxObject_GetSibling(orxObject_GetChild(scene))))))))));

	num_bombs = 5 - num_bombs;

	char BombsString[4];
	sprintf(BombsString, "%d", (int)num_bombs);
	orxObject_SetTextString(bombs_label, BombsString);

	return result;
}

void orxFASTCALL LoadScene(orxS32 screen_id)
{

	/* Delete the stage, if it exists */
	if (stage)
	{
		orxClock_Delete(orxClock_Get("WaveClock"));
		orxClock_Delete(orxClock_Get("HomingClock"));
		deleteBullets();
		deleteEnemies(orxFALSE);
		orxObject_SetLifeTime(stage, orxFLOAT_0);
		stage = orxNULL;
	}

	/* Delete the scene, if it exists */
	if (scene)
	{
		orxObject_SetLifeTime(scene, orxFLOAT_0);
		scene = orxNULL;
	}

	/* Delete the viewport, if it exists */
	if (viewport)
	{
		orxViewport_Delete(viewport);
		viewport = orxNULL;
	}

	new_scene = orxTRUE;

	/* Pushes the Scenes section */
	orxConfig_PushSection("Scenes");

	/* Load correct scene from list of scenes */
	if(orxConfig_Load(orxConfig_GetListString("ConfigList", screen_id)))
	{
		if(screen_id != 2)
		{
			/* Set new controls (Later, rework this so that we only have one control set, we don't need all of these!) */
			orxConfig_PushSection("Input");
			if (screen_id == 0 || screen_id == 5 || screen_id == 6 || screen_id == 7)
				orxInput_SelectSet("Discard");
			else
				orxInput_SelectSet(orxConfig_GetListString("SetList", screen_id));
			orxConfig_PopSection();

			/* Create a new scene and viewport */
			viewport = orxViewport_CreateFromConfig("Viewport");
			scene    = orxObject_CreateFromConfig("Scene");
		}
		else
			orxInput_SelectSet("Discard");
	}

	/* Pops the Scenes section */
	orxConfig_PopSection();

	switch(screen_id)
	{
		case 1:
			LoadMenu();
		break;

		case 2:
			LoadGame();
		break;

		case 5:
		case 6:
		{
			char FinalScoreString[4];
			sprintf(FinalScoreString, "%d", (int)score);
			orxObject_SetTextString(orxObject_GetChild(orxObject_GetSibling(orxObject_GetChild(scene))), FinalScoreString);
		}

			break;

		default:
			break;
	}
	return;
}

static void orxFASTCALL laser_spread(orxSPAWNER *spawner, orxOBJECT *shot)
{
	return;
}

static void orxFASTCALL wide_spread(orxSPAWNER *spawner, orxOBJECT *shot)
{
	/*static int inc = 0;
	float x_axis, total_span;
	static int num_threads = 3;
	orxVECTOR spreading;

	inc++;
	if (inc >= num_threads)
		inc = 0;
	orxSpawner_SetWaveSize(main_weapon, num_threads);

	static const int spacing = 100.f;
	total_span = num_threads * spacing;
	x_axis = (inc * spacing) - (total_span / 2.f);

	orxVector_Set(&spreading, x_axis, -500.f, 0);
	orxSpawner_SetObjectSpeed(main_weapon, &spreading);*/

	orxVECTOR speed;

	orxObject_SetRotation(shot,
		(((orxSpawner_GetTotalObjectCounter(spawner) - 1) % 4)
		* (orxMATH_KF_PI / 16)) - (3 * orxMATH_KF_PI / 32));

	orxObject_GetSpeed(shot, &speed);
	orxObject_SetRelativeSpeed(shot, &speed);

	return;
}

void orxFASTCALL homing_spread(orxSPAWNER *spawner, orxOBJECT *shot)
{
	addShotToLinkList(shot);
	return;
}

static orxOBJECT* SelectTarget(orxOBJECT *shot)
{
	orxVECTOR shotPosition,
			  enemyPosition;

	orxOBJECT  *closestEnemy = orxNULL;
	orxENEMY   *nodeSelected;

	orxFLOAT closestDistance,
			 currentDistance;

	if(orxLinkList_GetCounter(&enemyList) > 0)
	{
		nodeSelected = (orxENEMY *)orxLinkList_GetFirst(&enemyList);
		orxObject_GetWorldPosition(shot, &shotPosition);
		orxObject_GetWorldPosition(nodeSelected->enemy, &enemyPosition);
		closestDistance = orxVector_GetDistance(&enemyPosition, &shotPosition);

	if(enemyPosition.fX < 776.0f && enemyPosition.fX > 248.0 && enemyPosition.fY < 736 && enemyPosition.fY > 32)
	{
		closestEnemy = nodeSelected->enemy;

		//Loop throught the enemy's link list to find the closest distance
		do
		{
			// Gets the enemy's current possition
			orxObject_GetWorldPosition(nodeSelected->enemy, &enemyPosition);

			// Gets current enemy's posistion distance from the shot
			currentDistance = fabs(orxVector_GetDistance(&enemyPosition, &shotPosition));

			if (currentDistance < closestDistance)
			{
				closestDistance = orxVector_GetDistance(&enemyPosition, &shotPosition);
				closestEnemy = nodeSelected->enemy;
			}
		} while(nodeSelected = (orxENEMY *)orxLinkList_GetNext((orxLINKLIST_NODE *)nodeSelected), nodeSelected != orxNULL);
	}
	}
	return closestEnemy;
}

orxFLOAT GetCorrection(orxFLOAT diff, orxFLOAT MaxDiff, orxFLOAT PerfectDiff)
{
	if (fabs(diff) < MaxDiff)
	{
		if (fabs(PerfectDiff) - fabs(diff) > 0.0f)
			diff = PerfectDiff;
		else
		{
			diff = MaxDiff;
			if (PerfectDiff < 0.0f)
				diff *= -1;
		}
	}
	return diff;
}

void orxFASTCALL UpdateHomingTrajectory(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext )
{
	orxENEMY    *enemyNode;
	orxLINKLIST *shotList;
	orxSHOT     *shotNode;
	orxVECTOR  	shotPosition,target, bulletSpeed, newForce, oldSpeed, newSpeed;
	orxFLOAT	xDiff, yDiff, xPerfectDiff, yPerfectDiff;
	orxFLOAT	MaxDiff = 2, PercentageCorrection = 0.02;

	// get first enemy node from list to get the hash table
	if(orxLinkList_GetCounter(&enemyList) > 0)
	{
		enemyNode = (orxENEMY *)orxLinkList_GetFirst (&enemyList);
		do
		{
			// Get list from the hash table
			shotList = enemyNode->shotlist;

			// Get the first node from the shot list
			if(orxLinkList_GetCounter(shotList) > 0)
			{
				shotNode = (orxSHOT *)orxLinkList_GetFirst (shotList);
				// loop trhough the list obtained from the hash table
				do
				{
					orxObject_GetWorldPosition(enemyNode->enemy, &target);
					orxObject_GetWorldPosition(shotNode->shot, &shotPosition);

					if(shotPosition.fX < 776.0f && shotPosition.fX > 248.0 && shotPosition.fY < 736 && shotPosition.fY > 32)
					{
					bulletSpeed.fX = 500;
					bulletSpeed.fY = 500;
					bulletSpeed.fZ = 0;

					orxVector_Sub( &newForce, &target, &shotPosition );
					orxVector_Normalize( &newForce, &newForce);
					orxVector_Mul(&bulletSpeed, &newForce, &bulletSpeed);

					orxObject_GetSpeed(shotNode->shot, &oldSpeed);
					xPerfectDiff = bulletSpeed.fX - oldSpeed.fX;
					yPerfectDiff = bulletSpeed.fY - oldSpeed.fY;
					xDiff = ((xPerfectDiff) * PercentageCorrection);
					yDiff = ((yPerfectDiff) * PercentageCorrection);
					xDiff = GetCorrection(xDiff, MaxDiff, xPerfectDiff);
					xDiff = GetCorrection(xDiff, MaxDiff, xPerfectDiff);
					newSpeed.fX = (oldSpeed.fX + xDiff);
					newSpeed.fY = (oldSpeed.fY + yDiff);
					orxObject_SetSpeed(shotNode->shot, &newSpeed);

						orxObject_SetRotation(shotNode->shot,
							orxMATH_KF_PI - orxMath_ATan(newSpeed.fX, newSpeed.fY));
					}
				}
				while(shotNode = (orxSHOT *)orxLinkList_GetNext(( orxLINKLIST_NODE *)shotNode), shotNode != orxNULL);
			}
			}
		while(enemyNode = (orxENEMY *)orxLinkList_GetNext ((orxLINKLIST_NODE *)enemyNode), enemyNode != orxNULL);
	}
	return;
}

void orxFASTCALL addShotToLinkList(orxOBJECT *shot)
{
	orxENEMY *enemyNode;
	orxOBJECT *enemyTargeted;


	// Sellecting target
	enemyTargeted = SelectTarget(shot);

	//if enemy different than NULL then add shot to Enemy's shotlist
	if(enemyTargeted != orxNULL)
	{
		orxSHOT *llShot = (orxSHOT *)orxBank_Allocate(memBankShot);

		// Set the shot to its Node
		llShot->shot = shot;
		orxObject_SetUserData(shot, llShot);
		enemyNode = (orxENEMY *)orxObject_GetUserData(enemyTargeted);

		orxLinkList_AddEnd(enemyNode->shotlist, (orxLINKLIST_NODE *)llShot);
	}

	return;
}


static orxINLINE orxSTATUS HighlightMenuOption(orxS32 new_index)
{

	/* Remove the HighlightFX from the previous menu option. */
	orxObject_RemoveFX(menus[menu_index][option_index][0], "HighlightFX");
	orxObject_AddFX(menus[menu_index][option_index][0], "UnhighlightFX");

	orxConfig_PushSection(orxObject_GetName(menus[menu_index][0][0]));

	if(new_index > orxConfig_GetListCounter("ChildList"))
		new_index = 1;
	else if (new_index < 1)
		new_index = orxConfig_GetListCounter("ChildList");

	option_index = new_index;

	/* Add the HighlightFX to the newly selected menu option. */
	orxObject_RemoveFX(menus[menu_index][option_index][0], "UnhighlightFX"); // Do we need this? Check the logs to make sure it isn't failing.
	orxObject_AddFX(menus[menu_index][option_index][0], "HighlightFX");

	/* Set the description text to the description associated with the selected menu option. */
	orxObject_SetTextString(description,
			orxConfig_GetListString("DescriptionList", option_index - 1));
	orxConfig_PopSection();

	return orxSTATUS_SUCCESS;
}

static orxINLINE orxSTATUS HighlightMenuOptionEntry(orxBOOL forward)
{
	orxS32 entry_traverse;
	orxConfig_PushSection(orxObject_GetName(menus[menu_index][option_index][0]));

	switch(option_index)
	{

	/* Ship */
	case 1:
		if(forward)
			if(++ship_id >= orxConfig_GetListCounter("ChildList"))
				ship_id = orxConfig_GetListCounter("ChildList") - 1;
			else
			{
				for(entry_traverse = 1; entry_traverse <= orxConfig_GetListCounter("ChildList"); entry_traverse++)
				{
					orxObject_AddFX(menus[menu_index][option_index][entry_traverse], "MenuEntrySlideOutFX");
					if(ship_id + 1 == entry_traverse)
					{
						orxObject_RemoveFX(menus[menu_index][option_index][entry_traverse - 1], "MenuFadeInFX");
						orxObject_AddFX(menus   [menu_index][option_index][entry_traverse - 1], "MenuFadeOutFX");
						orxObject_RemoveFX(menus[menu_index][option_index][entry_traverse],     "MenuFadeOutFX");
						orxObject_AddFX(menus   [menu_index][option_index][entry_traverse],     "MenuFadeInFX");
					}
				}
			}
		else
			if(--ship_id < 0)
				ship_id = 0;
			else
			{
				for(entry_traverse = 1; entry_traverse <= orxConfig_GetListCounter("ChildList"); entry_traverse++)
				{
					orxObject_AddFX(menus[menu_index][option_index][entry_traverse], "MenuEntrySlideInFX");
					if(ship_id + 1 == entry_traverse)
					{
						orxObject_RemoveFX(menus[menu_index][option_index][entry_traverse + 1], "MenuFadeInFX");
						orxObject_AddFX(menus   [menu_index][option_index][entry_traverse + 1], "MenuFadeOutFX");
						orxObject_RemoveFX(menus[menu_index][option_index][entry_traverse],     "MenuFadeOutFX");
						orxObject_AddFX(menus   [menu_index][option_index][entry_traverse],     "MenuFadeInFX");
					}
				}
			}
		break;

	/* Stage */
	case 2:
		if(forward)
			if(++stage_id >= orxConfig_GetListCounter("ChildList"))
				stage_id = orxConfig_GetListCounter("ChildList") - 1;
			else
			{
				for(entry_traverse = 1; entry_traverse <= orxConfig_GetListCounter("ChildList"); entry_traverse++)
				{
					orxObject_AddFX(menus[menu_index][option_index][entry_traverse], "MenuEntrySlideOutFX");
					if(stage_id + 1 == entry_traverse)
					{
						orxObject_RemoveFX(menus[menu_index][option_index][entry_traverse - 1], "MenuFadeInFX");
						orxObject_AddFX(menus   [menu_index][option_index][entry_traverse - 1], "MenuFadeOutFX");
						orxObject_RemoveFX(menus[menu_index][option_index][entry_traverse],     "MenuFadeOutFX");
						orxObject_AddFX(menus   [menu_index][option_index][entry_traverse],     "MenuFadeInFX");
					}
				}
			}
		else
			if(--stage_id < 0)
				stage_id = 0;
			else
			{
				for(entry_traverse = 1; entry_traverse <= orxConfig_GetListCounter("ChildList"); entry_traverse++)
				{
					orxObject_AddFX(menus[menu_index][option_index][entry_traverse], "MenuEntrySlideInFX");
					if(stage_id + 1 == entry_traverse)
					{
						orxObject_RemoveFX(menus[menu_index][option_index][entry_traverse + 1], "MenuFadeInFX");
						orxObject_AddFX(menus   [menu_index][option_index][entry_traverse + 1], "MenuFadeOutFX");
						orxObject_RemoveFX(menus[menu_index][option_index][entry_traverse],     "MenuFadeOutFX");
						orxObject_AddFX(menus   [menu_index][option_index][entry_traverse],     "MenuFadeInFX");
					}
				}
			}
		break;

	/* Lives */
	case 3:
		if(forward)
			if(++num_lives >= orxConfig_GetListCounter("ChildList"))
				num_lives = orxConfig_GetListCounter("ChildList") - 1;
			else
			{
				for(entry_traverse = 1; entry_traverse <= orxConfig_GetListCounter("ChildList"); entry_traverse++)
				{
					orxObject_AddFX(menus[menu_index][option_index][entry_traverse], "MenuEntrySlideOutFX");
					if(num_lives + 1 == entry_traverse)
					{
						orxObject_RemoveFX(menus[menu_index][option_index][entry_traverse - 1], "MenuFadeInFX");
						orxObject_AddFX(menus   [menu_index][option_index][entry_traverse - 1], "MenuFadeOutFX");
						orxObject_RemoveFX(menus[menu_index][option_index][entry_traverse],     "MenuFadeOutFX");
						orxObject_AddFX(menus   [menu_index][option_index][entry_traverse],     "MenuFadeInFX");
					}
				}
			}
		else
			if(--num_lives < 0)
				num_lives = 0;
			else
			{
				for(entry_traverse = 1; entry_traverse <= orxConfig_GetListCounter("ChildList"); entry_traverse++)
				{
					orxObject_AddFX(menus[menu_index][option_index][entry_traverse], "MenuEntrySlideInFX");
					if(num_lives + 1 == entry_traverse)
					{
						orxObject_RemoveFX(menus[menu_index][option_index][entry_traverse + 1], "MenuFadeInFX");
						orxObject_AddFX(menus   [menu_index][option_index][entry_traverse + 1], "MenuFadeOutFX");
						orxObject_RemoveFX(menus[menu_index][option_index][entry_traverse],     "MenuFadeOutFX");
						orxObject_AddFX(menus   [menu_index][option_index][entry_traverse],     "MenuFadeInFX");
					}
				}
			}
		break;

	/* Bombs */
	case 4:
		if(forward)
			if(++num_bombs >= orxConfig_GetListCounter("ChildList"))
				num_bombs = orxConfig_GetListCounter("ChildList") - 1;
			else
			{
				for(entry_traverse = 1; entry_traverse <= orxConfig_GetListCounter("ChildList"); entry_traverse++)
				{
					orxObject_AddFX(menus[menu_index][option_index][entry_traverse], "MenuEntrySlideOutFX");
					if(num_bombs + 1 == entry_traverse)
					{
						orxObject_RemoveFX(menus[menu_index][option_index][entry_traverse - 1], "MenuFadeInFX");
						orxObject_AddFX(menus   [menu_index][option_index][entry_traverse - 1], "MenuFadeOutFX");
						orxObject_RemoveFX(menus[menu_index][option_index][entry_traverse],     "MenuFadeOutFX");
						orxObject_AddFX(menus   [menu_index][option_index][entry_traverse],     "MenuFadeInFX");
					}
				}
			}
		else
			if(--num_bombs < 0)
				num_bombs = 0;
			else
			{
				for(entry_traverse = 1; entry_traverse <= orxConfig_GetListCounter("ChildList"); entry_traverse++)
				{
					orxObject_AddFX(menus[menu_index][option_index][entry_traverse], "MenuEntrySlideInFX");
					if(num_bombs + 1 == entry_traverse)
					{
						orxObject_RemoveFX(menus[menu_index][option_index][entry_traverse + 1], "MenuFadeInFX");
						orxObject_AddFX(menus   [menu_index][option_index][entry_traverse + 1], "MenuFadeOutFX");
						orxObject_RemoveFX(menus[menu_index][option_index][entry_traverse],     "MenuFadeOutFX");
						orxObject_AddFX(menus   [menu_index][option_index][entry_traverse],     "MenuFadeInFX");
					}
				}
			}
		break;

	default:
		break;

	}

	orxConfig_PopSection();

	return orxSTATUS_SUCCESS;
}

/* Performs the selected menu option's "action" */
static orxINLINE orxSTATUS HandleMenuOption(orxBOOL previous)
{
	orxSTATUS result = orxSTATUS_FAILURE;

	if(previous)
	{
		orxConfig_PushSection(orxObject_GetName(menus[menu_index][0][0]));
		orxConfig_PushSection(orxConfig_GetListString("ChildList", orxConfig_GetListCounter("ChildList") - 1));
	}
	else
		orxConfig_PushSection(orxObject_GetName(menus[menu_index][option_index][0]));

	if(!orxString_Compare(orxConfig_GetString("Action"), "Advance"))
		SwitchMenu(orxConfig_GetS32("Menu_ID"));
	else if(!orxString_Compare(orxConfig_GetString("Action"), "ChangeScene"))
		LoadScene(orxConfig_GetS32("Scene_ID"));
	orxConfig_PopSection();

	if(previous)
		orxConfig_PopSection();

	return result;
}

void reassign_enemy(orxENEMY *enemyNode)
{
	orxSHOT   *shotNode, *clonedShot;
	orxOBJECT *newEnemy;
	orxENEMY  *newEnemyNode;

	if (orxLinkList_GetCounter(enemyNode->shotlist) > 0)
	{
		//Gets frist shot the shotlist of the enemy
		shotNode = (orxSHOT *)orxLinkList_GetFirst(enemyNode->shotlist);

		do
		{
			//Gets new target for the shot
			newEnemy = SelectTarget(shotNode->shot);

			if(newEnemy != orxNULL)
			{
				//Gets Enemy's node in the enemy's list
				newEnemyNode = (orxENEMY *)orxObject_GetUserData(newEnemy);

				clonedShot = (orxSHOT *)orxBank_Allocate(memBankShot);

				clonedShot->shot = shotNode->shot;
				orxObject_SetUserData(clonedShot->shot, clonedShot);

				// Adding cloned node to the new enemy's shot list
				orxLinkList_AddEnd (newEnemyNode->shotlist, (orxLINKLIST_NODE *)clonedShot);
			}

			if(orxLinkList_Remove((orxLINKLIST_NODE *)shotNode) == orxSTATUS_SUCCESS )
				orxLOG("'reassign_enemy' SHOT SUCESSFULLY REMOVED FROM OLD ENEMY'S SHOT LIST");
			else
				orxLOG("I COULDNT REMOVE THE NODE in 'REASSIGN_ENEMY'");
		} while(shotNode = (orxSHOT *)orxLinkList_GetFirst(enemyNode->shotlist), shotNode != orxNULL);
	}
	return;
}

/* Event handler*/
orxSTATUS orxFASTCALL EventHandler(const orxEVENT *_pstEvent)
{
	orxFX_EVENT_PAYLOAD *fx_info;
	orxPHYSICS_EVENT_PAYLOAD *physics_info;
	orxVECTOR position;

	/* Check for physics events. */
	if (_pstEvent->eType == orxEVENT_TYPE_PHYSICS)
	{
		/* Is a new contact? */
		if (_pstEvent->eID == orxPHYSICS_EVENT_CONTACT_ADD)
		{
			orxOBJECT *pstObject1 = orxNULL, *pstObject2 = orxNULL, *object = orxNULL;

			/* Gets colliding objects */
			pstObject1 = orxOBJECT(_pstEvent->hRecipient);
			pstObject2 = orxOBJECT(_pstEvent->hSender);

			if (pstObject1 != orxNULL && pstObject2 != orxNULL)
			{
				if (!orxString_NCompare(orxObject_GetName(pstObject1), "Screen", 6) || !orxString_NCompare(orxObject_GetName(pstObject2), "Screen", 6))
				{
					if (!orxString_NCompare(orxObject_GetName(pstObject2), "Screen", 6))
						object = pstObject1;
					else
						object = pstObject2;

					orxConfig_PushSection(orxObject_GetName(object));

					if (!orxString_Compare(orxConfig_GetString("Type"), "Shot"))
					{
						if (!orxString_Compare(orxSpawner_GetName(main_weapon), "BlueSpawner") && ((orxSHOT *)orxObject_GetUserData(object) != orxNULL))
						{
							orxSHOT *shotNode;
							shotNode = (orxSHOT *)orxObject_GetUserData(object);
							orxLinkList_Remove((orxLINKLIST_NODE *)shotNode);
							orxObject_SetLifeTime(object, orxFLOAT_0);
						}
						else
							orxObject_SetLifeTime(object, orxFLOAT_0);
					}
					else if (!orxString_Compare(orxConfig_GetString("Type"), "Enemy"))
					{

						orxENEMY *enemyNode;
						enemyNode = (orxENEMY *)orxObject_GetUserData(object);


						orxLinkList_Remove((orxLINKLIST_NODE *)enemyNode);
						orxObject_SetLifeTime(object, orxFLOAT_0);

						if (!orxString_Compare(orxSpawner_GetName(main_weapon), "BlueSpawner"))
							reassign_enemy(enemyNode);
					}
					else if (!orxString_Compare(orxConfig_GetString("Type"), "Bullet"))
					{
						orxBULLET *bulletNode;
						bulletNode = (orxBULLET *)orxObject_GetUserData(object);

						orxLinkList_Remove((orxLINKLIST_NODE *)bulletNode);

						orxObject_SetLifeTime(object, orxFLOAT_0);
					}
					orxConfig_PopSection();
				}

				/* On hit, delete all bullet objects. */
				else if (!orxString_NCompare(orxObject_GetName(pstObject1), "HUD", 3) ||
						 !orxString_NCompare(orxObject_GetName(pstObject2), "HUD", 3))
				{
				}
				else
				{
					if (!orxString_Compare(orxObject_GetName(pstObject2), "Ship") ||
						!orxString_Compare(orxObject_GetName(pstObject1), "Ship"))
					{
						orxOBJECT *ship = orxString_Compare(orxObject_GetName(pstObject2), "Ship") ? pstObject1 : pstObject2;
						physics_info = (orxPHYSICS_EVENT_PAYLOAD *)_pstEvent->pstPayload;

						if(!orxString_Compare("Hitbox", (ship == pstObject2) ? physics_info->zSenderPartName : physics_info->zRecipientPartName))
						{
							/*subtracting life*/
							if (num_lives-- == 0)
								LoadScene(5);
							else
						{
							char LivesString[4];
							position.fX = 512.0;
							position.fY = 512.0;
							position.fZ = 80.0;

							orxObject_SetPosition(ship, &position);

								sprintf(LivesString, "%d", (int)num_lives);

							orxObject_SetTextString(lives_label, LivesString);

							/* Delete all the bullets */
							deleteBullets();
						}
					}
					}
					else if (!orxString_Compare(orxObject_GetName(pstObject1), "Shot") ||
							 !orxString_Compare(orxObject_GetName(pstObject2), "Shot"))
					{

						orxOBJECT *shot;
						orxOBJECT *enemy;
						orxSHOT   *shotNode;

						if (!orxString_Compare(orxObject_GetName(pstObject2), "Shot"))
						{
							shot = pstObject2;
							enemy = pstObject1;
						}
						else
						{
							shot = pstObject1;
							enemy = pstObject2;
						}
						char ScoreString[11];

						orxConfig_PushSection(orxObject_GetName(shot));

						if (!orxString_Compare(orxSpawner_GetName(main_weapon), "BlueSpawner") && (orxObject_GetUserData(shot) != orxNULL))
						{
							shotNode = (orxSHOT *)orxObject_GetUserData(shot);
							orxLinkList_Remove((orxLINKLIST_NODE *)shotNode);
						}
						orxObject_SetLifeTime(shot, orxFLOAT_0);

						orxConfig_PushSection(orxObject_GetName(enemy));
						if ((((orxENEMY *)orxObject_GetUserData(enemy))->health -= 1/*orxConfig_GetS32("Strength")*/) <= 0)
						{

							if ((--((orxENEMY *)orxObject_GetUserData(enemy))->bars) <= 0)
							{
								if (orxConfig_GetS32("Bars") <= 1)
								{ // Enemy Death
									score += orxConfig_GetS32("Health");
									sprintf(ScoreString, "%010d", (int)score);
									orxObject_SetTextString(score_label, ScoreString);
								}
								else
								{ // Boss Death
									orxCOLOR invisible;
									orxObject_GetColor(bossHealthBars[0], &invisible);
									invisible.fAlpha = 0.0f;
									orxObject_SetColor(bossHealthBars[0], &invisible);

									score += orxConfig_GetS32("Health") * (orxConfig_GetS32("Bars") + 1);
									sprintf(ScoreString, "%010d", (int)score);
									orxObject_SetTextString(score_label, ScoreString);

									deleteBullets();

									LoadScene(6);
								}

								orxENEMY *enemyNode = (orxENEMY *)orxObject_GetUserData(enemy);

								orxLinkList_Remove((orxLINKLIST_NODE *)enemyNode);

								orxObject_SetLifeTime(enemy, orxFLOAT_0);

								if (!orxString_Compare(orxSpawner_GetName(main_weapon), "BlueSpawner"))
									reassign_enemy(enemyNode);
							}
							else
							{ // Boss Pattern Change
								orxCOLOR invisible;
								orxObject_GetColor(bossHealthBars[0], &invisible);
								invisible.fAlpha = 0.0f;

								((orxENEMY *)orxObject_GetUserData(enemy))->health = orxConfig_GetS32("Health");

								enableBossPattern(4 - ((orxENEMY *)orxObject_GetUserData(enemy))->bars - 1, orxFALSE);

								if (((orxENEMY *)orxObject_GetUserData(enemy))->bars > 0)
									enableBossPattern(4 - ((orxENEMY *)orxObject_GetUserData(enemy))->bars, orxTRUE);

								orxObject_SetColor(bossHealthBars[((orxENEMY *)orxObject_GetUserData(enemy))->bars], &invisible);

								score += orxConfig_GetS32("Health");
								sprintf(ScoreString, "%010d", (int)score);
								orxObject_SetTextString(score_label, ScoreString);

								deleteBullets();
							}
						}
						else
						{
							if (orxConfig_GetS32("Bars") == 4)
							{
								scale.fX = (((orxENEMY *)orxObject_GetUserData(enemy))->health / (orxFLOAT)orxConfig_GetS32("Health")) * 512;
								orxObject_SetScale(bossHealthBars[((orxENEMY *)orxObject_GetUserData(enemy))->bars - 1], &scale);
							}
						}
						orxConfig_PopSection();
						orxConfig_PopSection();
					}
				}
			}
		}
	}

	/* Check for FX events. */
	if (_pstEvent->eType == orxEVENT_TYPE_FX)
	{
		/* Check to see if an FX has stopped. */
		if (_pstEvent->eID == orxFX_EVENT_STOP)
		{
			fx_info = (orxFX_EVENT_PAYLOAD *)_pstEvent->pstPayload;

			if (!orxString_Compare(fx_info->zFXName, "PressZFX"))
				orxInput_SelectSet("SkipOuttro");

			if (!orxString_Compare(fx_info->zFXName, "FadeFX4"))
				orxInput_SelectSet("SkipIntro");

			if (!orxString_Compare(fx_info->zFXName, "FadeExit"))
				orxEvent_SendShort(orxEVENT_TYPE_SYSTEM, orxSYSTEM_EVENT_CLOSE);

			if(!orxString_Compare(fx_info->zFXName, "TutorialFX"))
				orxInput_SelectSet("StartGame");
		}

		if (_pstEvent->eID == orxFX_EVENT_ADD)
		{
			fx_info = (orxFX_EVENT_PAYLOAD *) _pstEvent->pstPayload;
			//affected_object = orxOBJECT(_pstEvent->hRecipient);
		}

		if (_pstEvent->eID == orxFX_EVENT_REMOVE)
		{
			fx_info = (orxFX_EVENT_PAYLOAD *) _pstEvent->pstPayload;
			//affected_object = orxOBJECT(_pstEvent->hRecipient);
		}

		if (_pstEvent->eID == orxFX_EVENT_START)
		{
			fx_info = (orxFX_EVENT_PAYLOAD *) _pstEvent->pstPayload;
			//affected_object = orxOBJECT(_pstEvent->hRecipient);
		}
	}

	/* Check for input events. */
//	if (_pstEvent->eType == orxEVENT_TYPE_INPUT)
//	{
//		if (_pstEvent->eID == orxINPUT_EVENT_ON)
//			input_info = (orxINPUT_EVENT_PAYLOAD *) _pstEvent->pstPayload;
//		if (_pstEvent->eID == orxINPUT_EVENT_OFF)
//			input_info = (orxINPUT_EVENT_PAYLOAD *) _pstEvent->pstPayload;
//	}

	/* Check for spawner events. */
	if (_pstEvent->eType == orxEVENT_TYPE_SPAWNER)
	{
		orxSPAWNER *sender = orxSPAWNER(_pstEvent->hSender);
		orxOBJECT  *recipient = orxOBJECT(_pstEvent->hRecipient);

		if(_pstEvent->eID == orxSPAWNER_EVENT_SPAWN)
		{
			orxConfig_PushSection(orxObject_GetName(recipient));
			if (!orxString_Compare(orxConfig_GetString("Type"), "Enemy"))
			{
				orxLINKLIST *shotLinkList = (orxLINKLIST *)orxBank_Allocate(memBankLinkedList);
				orxMemory_Zero(shotLinkList, sizeof(orxLINKLIST));
				orxENEMY *llEnemy = (orxENEMY *)orxBank_Allocate(memBankEnemy);
				llEnemy->enemy = recipient;
				llEnemy->health = orxConfig_GetS32("Health");
				llEnemy->shotlist = shotLinkList;
				llEnemy->bars   = orxConfig_GetS32("Bars");
				orxObject_SetUserData(recipient, llEnemy);
				orxLinkList_AddEnd(&enemyList, (orxLINKLIST_NODE *)llEnemy);
			}
			else if (!orxString_Compare(orxConfig_GetString("Type"), "Bullet"))
			{
				orxBULLET *llBullet = (orxBULLET *)orxBank_Allocate(memBankBullet);
				llBullet->bullet = recipient;
				orxObject_SetUserData(recipient, llBullet);
				orxLinkList_AddEnd(&bulletList, (orxLINKLIST_NODE *)llBullet);
			}
			else if (!orxString_Compare(orxConfig_GetString("Type"), "Shot"))
				shoot_type(sender, recipient);
			orxConfig_PopSection();
		}
	}
	return orxSTATUS_SUCCESS;
}

orxSTATUS orxFASTCALL StageOneHandler(const orxEVENT *_pstEvent)
{
	switch(_pstEvent->eType)
	{
		case orxEVENT_TYPE_SPAWNER:
		{
			orxSPAWNER* spawner        = (orxSPAWNER*)_pstEvent->hSender;
			orxOBJECT*  object_spawned = (orxOBJECT*)_pstEvent->hRecipient;
			orxVECTOR   position,
					    player_position;
			const orxSTRING spawner_name = orxSpawner_GetName(spawner);

			/* Quick optimization: Call GetName one time and store it in a variable, then check that variable */
			/* instead of calling that function every single time there is an if statement. */

			switch(_pstEvent->eID)
			{
				case orxSPAWNER_EVENT_CREATE:
				{
					if(!orxString_Compare(spawner_name, "W12Spawner") ||
					   !orxString_Compare(spawner_name, "W13Spawner") ||
					   !orxString_Compare(spawner_name, "W14Spawner") ||
					   !orxString_Compare(spawner_name, "W15Spawner"))
					{
						orxObject_GetWorldPosition(player, &player_position);
						orxSpawner_GetPosition(spawner, &position);

						orxVector_Set(&position, player_position.fX, position.fY, position.fZ);

						orxSpawner_SetPosition(spawner, &position);
					}
					break;
				}
				case orxSPAWNER_EVENT_SPAWN:
				{
					if(!orxString_Compare(spawner_name, "FireAtPlayer") ||
					   !orxString_Compare(spawner_name, "Fire4AtPlayer"))
					{
						orxVECTOR newForce,
								  bulletSpeed,
								  target;

						orxObject_GetPosition(player, &target);
						orxObject_GetPosition(object_spawned, &position);

						bulletSpeed.fX = 128;
						bulletSpeed.fY = 128;
						bulletSpeed.fZ = 0;

						orxVector_Sub( &newForce, &target, &position );
						orxVector_Normalize( &newForce, &newForce);
						orxVector_Mul(&bulletSpeed, &newForce, &bulletSpeed);
						orxObject_SetSpeed(object_spawned, &bulletSpeed);
					}
					else if(!orxString_Compare(spawner_name, "FirinMahLazor"))
					{
						orxVECTOR newForce,
								  bulletSpeed,
								  target;

						orxObject_GetPosition(player, &target);
						orxObject_GetPosition(object_spawned, &position);

						bulletSpeed.fX = 300;
						bulletSpeed.fY = 300;
						bulletSpeed.fZ = 0;

						orxVector_Sub( &newForce, &target, &position );
						orxVector_Normalize( &newForce, &newForce);
						orxVector_Mul(&bulletSpeed, &newForce, &bulletSpeed);
						orxObject_SetSpeed(object_spawned, &bulletSpeed);
					}
					else if(!orxString_NCompare(spawner_name, "FireCircle", 10))
					{
						orxSpawner_SetWaveDelay(spawner, 1.5);
						orxSpawner_SetRotation(spawner, (orxSpawner_GetTotalObjectCounter(spawner) * orxMATH_KF_PI) / 5);
					}
					else if(!orxString_Compare(spawner_name, "Fire5Circles"))
					{
						orxVECTOR bullet_speed;
						orxS32    y_speed;

						orxSpawner_SetRotation(spawner, (orxSpawner_GetTotalObjectCounter(spawner) * orxMATH_KF_PI) / 8);

						switch(((orxSpawner_GetTotalObjectCounter(spawner) - 1) / 16) % 5)
						{
							case 0:
								y_speed = 150;
								break;
							case 1:
								y_speed = 100;
								break;
							case 2:
								y_speed = 200;
								break;
							case 3:
								y_speed = 175;
								break;
							case 4:
								y_speed = 275;
								break;
							case 5:
								y_speed = 250;
								break;
						}

						orxVector_Set(&bullet_speed, 0, y_speed, 0);
						orxObject_SetRelativeSpeed(object_spawned, &bullet_speed);
					}
					else if(!orxString_Compare(spawner_name, "LazySpiral"))
						orxSpawner_SetRotation(spawner, (orxSpawner_GetTotalObjectCounter(spawner) % 20 * orxMATH_KF_PI) / 10);
					else if(!orxString_Compare(spawner_name, "AlternatingCircle"))
					{
						orxSpawner_SetRotation(spawner,
							(((orxSpawner_GetTotalObjectCounter(spawner) / 64) % 2) * (orxMATH_KF_PI / 16)) +
							(((orxSpawner_GetTotalObjectCounter(spawner) % 8) * (orxMATH_KF_PI / 128)) - (orxMATH_KF_PI /32)) +
							(orxSpawner_GetTotalObjectCounter(spawner) / 8) * (orxMATH_KF_PI / 8));
					}
					else if(!orxString_Compare(spawner_name, "ShoopDaWoop"))
					{
						orxVECTOR newForce,
								  bulletSpeed,
								  target;

						orxObject_GetPosition(player, &target);
						orxObject_GetPosition(object_spawned, &position);

						bulletSpeed.fX = 200;
						bulletSpeed.fY = 200;
						bulletSpeed.fZ = 0;

						orxSpawner_SetRotation(spawner,
							(orxSpawner_GetTotalObjectCounter(spawner) % 2 ? 1 : -1) * (.5f / ((
							(orxSpawner_GetTotalObjectCounter(spawner) - 1) % 8) / 2 + 1)));

						orxVector_Sub( &newForce, &target, &position );
						orxVector_Normalize( &newForce, &newForce);
						orxVector_Mul(&bulletSpeed, &newForce, &bulletSpeed);
						orxObject_SetRelativeSpeed(object_spawned, &bulletSpeed);
					}
					else if(!orxString_Compare(spawner_name, "W1Spawner") ||
							!orxString_Compare(spawner_name, "W2Spawner") ||
							!orxString_Compare(spawner_name, "W3Spawner"))
					{
						switch(orxSpawner_GetTotalObjectCounter(spawner))
						{
							case 1:
								position.fY = -128;
								position.fZ = 0;
								position.fX = (!orxString_Compare(spawner_name, "W2Spawner")) ? -32 : 32;
								orxSpawner_SetPosition(spawner, &position);

								if(!orxString_Compare(spawner_name, "W1Spawner"))
									orxObject_AddDelayedFX(object_spawned, "W1EnterFX", 0.25f);
								else if(!orxString_Compare(spawner_name, "W2Spawner"))
									orxObject_AddDelayedFX(object_spawned, "W2EnterFX", 0.25f);
								else if(!orxString_Compare(spawner_name, "W3Spawner"))
									orxObject_AddDelayedFX(object_spawned, "W3EnterFX", 0.25f);
								orxSpawner_SetWaveDelay(orxOBJECT_GET_STRUCTURE(object_spawned, SPAWNER), 1.25f);
								break;
							case 2:
								position.fY = -128;
								position.fZ = 0;
								position.fX = (!orxString_Compare(spawner_name, "W2Spawner")) ? 32 : -32;

								orxSpawner_SetPosition(spawner, &position);

								if(!orxString_Compare(spawner_name, "W1Spawner"))
									orxObject_AddDelayedFX(object_spawned, "W1EnterFX", 0.5f);
								else if(!orxString_Compare(spawner_name, "W2Spawner"))
									orxObject_AddDelayedFX(object_spawned, "W2EnterFX", 0.5f);
								else if(!orxString_Compare(spawner_name, "W3Spawner"))
									orxObject_AddDelayedFX(object_spawned, "W3EnterFX", 0.5f);
								orxSpawner_SetWaveDelay(orxOBJECT_GET_STRUCTURE(object_spawned, SPAWNER), 1.5f);
								break;
							default:
								if(!orxString_Compare(spawner_name, "W1Spawner"))
									orxObject_AddFX(object_spawned, "W1EnterFX");
								else if(!orxString_Compare(spawner_name, "W2Spawner"))
									orxObject_AddFX(object_spawned, "W2EnterFX");
								else if(!orxString_Compare(spawner_name, "W3Spawner"))
									orxObject_AddFX(object_spawned, "W3EnterFX");
								break;
						}
					}
					else if(!orxString_Compare(spawner_name, "W4Spawner"))
					{
						position.fX = (64 * orxSpawner_GetTotalObjectCounter(spawner));
						position.fY = -120.0f;
						position.fZ = 0.f;
						orxSpawner_SetPosition(spawner, &position);
						orxObject_AddDelayedFX(object_spawned, "W4EnterFX", 0.25f * orxSpawner_GetTotalObjectCounter(spawner));
						orxSpawner_SetWaveDelay(orxOBJECT_GET_STRUCTURE(object_spawned, SPAWNER), 1 + 0.25f * orxSpawner_GetTotalObjectCounter(spawner));
					}
					else if(!orxString_Compare(spawner_name, "W5Spawner"))
						orxObject_AddDelayedFX(object_spawned, "W5EnterFX", 0.5f * orxSpawner_GetTotalObjectCounter(spawner));
					else if(!orxString_Compare(spawner_name, "W6Spawner"))
						orxObject_AddDelayedFX(object_spawned, "W6EnterFX", 0.5f * orxSpawner_GetTotalObjectCounter(spawner));
					else if(!orxString_Compare(spawner_name, "W7Spawner"))
					{
						position.fX = -((105.6 * 2 * orxSpawner_GetTotalObjectCounter(spawner)));
						position.fY = 0.f;
						position.fZ = 0.f;

						orxSpawner_SetPosition(spawner, &position);
						orxObject_AddDelayedFX(object_spawned, "W7EnterFX", 0.25f * orxSpawner_GetTotalObjectCounter(spawner));

						orxSpawner_SetWaveDelay(orxOBJECT_GET_STRUCTURE(object_spawned, SPAWNER), 1.25f + .55f * orxSpawner_GetTotalObjectCounter(spawner));
					}
					else if(!orxString_Compare(spawner_name, "W8Spawner"))
					{
						position.fX = ((158.4 * orxSpawner_GetTotalObjectCounter(spawner)));
						position.fY = 0.f;
						position.fZ = 0.f;

						orxSpawner_SetPosition(spawner, &position);
						orxObject_AddDelayedFX(object_spawned, "W8EnterFX", 0.25f * orxSpawner_GetTotalObjectCounter(spawner));
					}
					else if(!orxString_Compare(spawner_name, "W9Spawner"))
					{
						position.fX = -((105.6 * 2 * orxSpawner_GetTotalObjectCounter(spawner)));
						position.fY = 0.f;
						position.fZ = 0.f;

						orxSpawner_SetPosition(spawner, &position);
						orxObject_AddDelayedFX(object_spawned, "W9EnterFX", 0.25f * orxSpawner_GetTotalObjectCounter(spawner));

						orxSpawner_SetWaveDelay(orxOBJECT_GET_STRUCTURE(object_spawned, SPAWNER), 1.25f + .55f * orxSpawner_GetTotalObjectCounter(spawner));
					}
					else if(!orxString_Compare(spawner_name, "W10Spawner"))
						orxObject_AddDelayedFX(object_spawned, "W10EnterFX", 0.45f * orxSpawner_GetTotalObjectCounter(spawner));
					else if(!orxString_Compare(spawner_name, "W11Spawner"))
						orxObject_AddDelayedFX(object_spawned, "W11EnterFX", 0.45f * orxSpawner_GetTotalObjectCounter(spawner));
					else if (!orxString_Compare(spawner_name, "W12Spawner") ||
							 !orxString_Compare(spawner_name, "W13Spawner") ||
							 !orxString_Compare(spawner_name, "W14Spawner") ||
							 !orxString_Compare(spawner_name, "W15Spawner"))
					{
						switch(orxSpawner_GetTotalObjectCounter(spawner))
						{
							case 1:
								orxSpawner_GetPosition(spawner, &position);

								position.fX += 32;
								position.fY -= 32;

								orxSpawner_SetPosition(spawner, &position);
								orxObject_AddDelayedFX(object_spawned, "W12EnterFX", 0.25f);
								orxSpawner_SetWaveDelay(orxOBJECT_GET_STRUCTURE(object_spawned, SPAWNER), 0.25f);
								break;
							case 2:
								orxSpawner_GetPosition(spawner, &position);

								position.fX -= 64;

								orxSpawner_SetPosition(spawner, &position);
								orxObject_AddDelayedFX(object_spawned, "W12EnterFX", 0.25f);
								orxSpawner_SetWaveDelay(orxOBJECT_GET_STRUCTURE(object_spawned, SPAWNER), 0.25f);
								break;
							default:
								orxObject_AddFX(object_spawned, "W12EnterFX");
								break;
						}
					}
					else if(!orxString_Compare(spawner_name, "W16Spawner"))
						orxObject_AddDelayedFX(object_spawned, "W16EnterFX", 0.75 * orxSpawner_GetTotalObjectCounter(spawner));
					else if(!orxString_Compare(spawner_name, "W17Spawner"))
						orxObject_AddDelayedFX(object_spawned, "W17EnterFX", 0.75 * orxSpawner_GetTotalObjectCounter(spawner));
					else if(!orxString_Compare(spawner_name, "W18Spawner"))
						orxObject_AddFX(object_spawned, "W18EnterFX");

					break;
				}
			}
			break;
		}
		case orxEVENT_TYPE_FX:
		{
			orxFX_EVENT_PAYLOAD *fx_info = (orxFX_EVENT_PAYLOAD *)_pstEvent->pstPayload;
			orxOBJECT *object = (orxOBJECT *)_pstEvent->hRecipient;

			if (!orxString_Compare(fx_info->zFXName, "W19FX"))
			{
				switch(_pstEvent->eID)
				{
					case orxFX_EVENT_START:
					{
						int index;
						orxOBJECT *boss = (orxOBJECT*)_pstEvent->hRecipient;
						orxCOLOR visible;

						orxOBJECT *pattern = orxObject_GetChild(bossPatterns[0] = orxObject_GetChild(boss));
						for (index = 0; index < 4; index++)
						{
							orxObject_GetColor(bossHealthBars[index], &visible);
							visible.fAlpha = 1.0f;
							orxObject_SetColor(bossHealthBars[index], &visible);

							do
								orxSpawner_Enable(orxOBJECT_GET_STRUCTURE(pattern, SPAWNER), orxFALSE);
							while ((pattern = orxObject_GetSibling(pattern)) != orxNULL);

							if (index < 3)
								pattern = orxObject_GetChild(bossPatterns[index+1] = orxObject_GetSibling(bossPatterns[index]));
						}

						break;
					}
					case orxFX_EVENT_STOP:
					{
						enableBossPattern(0, orxTRUE);
						break;
					}
				}
			}
			else if (!orxString_Compare(fx_info->zFXName, "W18EnterFX") && _pstEvent->eID == orxFX_EVENT_START)
				orxObject_SetVolume(stage, 0.0f);

			switch(_pstEvent->eID)
			{
				case orxFX_EVENT_STOP:
				{
					if(!orxString_NCompare(fx_info->zFXName, "W", 1))
					{
						if(!orxString_Compare(fx_info->zFXName, "W1EnterFX"))
							orxObject_AddFX(object, "W1ExitFX");
						else if(!orxString_Compare(fx_info->zFXName, "W2EnterFX"))
							orxObject_AddFX(object, "W2ExitFX");
						else if(!orxString_Compare(fx_info->zFXName, "W3EnterFX"))
							orxObject_AddFX(object, "W3ExitFX");
						else if(!orxString_Compare(fx_info->zFXName, "W4EnterFX"))
							orxObject_AddFX(object, "W4ExitFX");
						else if(!orxString_Compare(fx_info->zFXName, "W5EnterFX"))
							orxObject_AddFX(object, "W5ExitFX");
						else if(!orxString_Compare(fx_info->zFXName, "W6EnterFX"))
							orxObject_AddFX(object, "W6ExitFX");
						else if(!orxString_Compare(fx_info->zFXName, "W7EnterFX"))
							orxObject_AddDelayedFX(object, "W7ExitFX", 3.0f);
						else if(!orxString_Compare(fx_info->zFXName, "W8EnterFX"))
							orxObject_AddDelayedFX(object, "W8ExitFX", 3.0f);
						else if(!orxString_Compare(fx_info->zFXName, "W9EnterFX"))
							orxObject_AddDelayedFX(object, "W9ExitFX", 3.0f);
						else if(!orxString_Compare(fx_info->zFXName, "W10EnterFX"))
							orxObject_AddFX(object, "W10ExitFX");
						else if(!orxString_Compare(fx_info->zFXName, "W11EnterFX"))
							orxObject_AddFX(object, "W11ExitFX");
						else if(!orxString_Compare(fx_info->zFXName, "W12EnterFX"))
							orxObject_AddFX(object, "W12ExitFX");
						else if(!orxString_Compare(fx_info->zFXName, "W13EnterFX"))
							orxObject_AddFX(object, "W13ExitFX");
						else if(!orxString_Compare(fx_info->zFXName, "W14EnterFX"))
							orxObject_AddFX(object, "W14ExitFX");
						else if(!orxString_Compare(fx_info->zFXName, "W15EnterFX"))
							orxObject_AddFX(object, "W15ExitFX");
						else if(!orxString_Compare(fx_info->zFXName, "W16EnterFX"))
							orxObject_AddFX(object, "W16ExitFX");
						else if(!orxString_Compare(fx_info->zFXName, "W17EnterFX"))
							orxObject_AddFX(object, "W17ExitFX");
						else if(!orxString_Compare(fx_info->zFXName, "W18EnterFX"))
							orxObject_AddFX(object, "W18ExitFX");
					}
				}
				break;
			}
			break;
		}
		default:
			break;
	}
	return orxSTATUS_SUCCESS;
}

orxSTATUS orxFASTCALL StageThreeHandler(const orxEVENT *_pstEvent)
{
	switch(_pstEvent->eType)
	{
		case orxEVENT_TYPE_SPAWNER:
		{
			orxSPAWNER* spawner = (orxSPAWNER*)_pstEvent->hSender;
			orxOBJECT*  object_spawned = (orxOBJECT*)_pstEvent->hRecipient;
			orxVECTOR position;
			const orxSTRING spawnerName = orxSpawner_GetName(spawner);

			switch(_pstEvent->eID)
			{
				case orxSPAWNER_EVENT_SPAWN:
				{
					if(!orxString_Compare(spawnerName, "FireArcsAtPlayerALot"))
					{
						orxVECTOR newForce,
								  bulletSpeed,
								  target;

						orxObject_GetPosition(player, &target);
						orxObject_GetPosition(object_spawned, &position);

						bulletSpeed.fX = 256;
						bulletSpeed.fY = 256;
						bulletSpeed.fZ = 0;

						orxObject_SetRotation(object_spawned,
							((orxSpawner_GetTotalObjectCounter(spawner) % 8) * .05) - .175);

						orxVector_Sub(&newForce, &target, &position);
						orxVector_Normalize(&newForce, &newForce);
						orxVector_Mul(&bulletSpeed, &newForce, &bulletSpeed);
						orxObject_SetRelativeSpeed(object_spawned, &bulletSpeed);
					}
					else if(!orxString_Compare(spawnerName, "FireSpiral") ||
							!orxString_Compare(spawnerName, "FireSwirlClockwise"))
						orxSpawner_SetRotation(spawner, // divide by which wave to provide swirl, mod then multiply to det angle slot
							(((orxSpawner_GetTotalObjectCounter(spawner) % 8) * orxMATH_KF_PI) / 4)
							+ (((orxSpawner_GetTotalObjectCounter(spawner) - 1) / 8) * .05));
					else if(!orxString_Compare(spawnerName, "FireSwirlCounterClockwise"))
						orxSpawner_SetRotation(spawner, // divide by which wave to provide swirl, mod then multiply to det angle slot
							(((orxSpawner_GetTotalObjectCounter(spawner) % 8) * orxMATH_KF_PI) / 4)
							- (((orxSpawner_GetTotalObjectCounter(spawner) - 1) / 8) * .05));
					else if(!orxString_Compare(spawnerName, "FireAtPlayer"))
					{
						orxVECTOR newForce,
								  bulletSpeed,
								  target;

						orxObject_GetPosition(player, &target);
						orxObject_GetPosition(object_spawned, &position);

						bulletSpeed.fX = 192;
						bulletSpeed.fY = 192;
						bulletSpeed.fZ = 0;

						orxVector_Sub(&newForce, &target, &position);
						orxVector_Normalize(&newForce, &newForce);
						orxVector_Mul(&bulletSpeed, &newForce, &bulletSpeed);
						orxObject_SetSpeed(object_spawned, &bulletSpeed);
					}
					else if(!orxString_Compare(spawnerName, "FireAtPlayerALot"))
					{
						orxVECTOR newForce,
								  bulletSpeed,
								  target;

						orxObject_GetPosition(player, &target);
						orxObject_GetPosition(object_spawned, &position);

						bulletSpeed.fX = 256;
						bulletSpeed.fY = 256;
						bulletSpeed.fZ = 0;

						orxVector_Sub(&newForce, &target, &position);
						orxVector_Normalize(&newForce, &newForce);
						orxVector_Mul(&bulletSpeed, &newForce, &bulletSpeed);
						orxObject_SetSpeed(object_spawned, &bulletSpeed);
					}
					else if(!orxString_Compare(spawnerName, "W1Spawner"))
						orxObject_AddDelayedFX(object_spawned, "W1FX", 0.5f);
					else if(!orxString_Compare(spawnerName, "W2Spawner"))
						orxObject_AddDelayedFX(object_spawned, "W2FX", 0.5f);
					else if(!orxString_Compare(spawnerName, "W3Spawner"))
						orxObject_AddDelayedFX(object_spawned, "W3FX", 0.5f);
					else if(!orxString_Compare(spawnerName, "W4Spawner"))
						orxObject_AddDelayedFX(object_spawned, "W4FX", 0.5f);
					else if(!orxString_Compare(spawnerName, "W5Spawner"))
						orxObject_AddDelayedFX(object_spawned, "W5FX", 0.5f);
					else if(!orxString_Compare(spawnerName, "W6Spawner"))
						orxObject_AddDelayedFX(object_spawned, "W6FX", 0.5f);
					else if(!orxString_Compare(spawnerName, "W7Spawner"))
						orxObject_AddDelayedFX(object_spawned, "W7FX", 0.5f);
					else if(!orxString_Compare(spawnerName, "W8Spawner"))
						orxObject_AddFX       (object_spawned, "W8FX");
					else if(!orxString_Compare(spawnerName, "W9Spawner"))
						orxObject_AddFX       (object_spawned, "W9FX");
					else if(!orxString_Compare(spawnerName, "W10Spawner"))
						orxObject_AddDelayedFX(object_spawned, "W10FX", 0.5f);
					else if(!orxString_Compare(spawnerName, "W13Spawner"))
						orxObject_AddDelayedFX(object_spawned, "W13FX", 0.5f);
					else if(!orxString_Compare(spawnerName, "W14Spawner"))
						orxObject_AddDelayedFX(object_spawned, "W14FX", 0.5f);
					else if(!orxString_Compare(spawnerName, "W15Spawner"))
						orxObject_AddDelayedFX(object_spawned, "W15FX", 0.5f);
					else if(!orxString_Compare(spawnerName, "W16Spawner"))
						orxObject_AddDelayedFX(object_spawned, "W16FX", 0.5f);
					else if(!orxString_Compare(spawnerName, "W17Spawner"))
						orxObject_AddFX       (object_spawned, "W17FX");
					else if(!orxString_Compare(spawnerName, "W18Spawner"))
						orxObject_AddFX       (object_spawned, "W18FX");
					else if(!orxString_Compare(spawnerName, "W19Spawner"))
						orxObject_AddFX       (object_spawned, "W19FX");
					else if(!orxString_Compare(spawnerName, "W20Spawner"))
						orxObject_AddFX       (object_spawned, "W20FX");
					else if(!orxString_Compare(spawnerName, "W21Spawner"))
						orxObject_AddFX       (object_spawned, "W21FX");
					else if(!orxString_Compare(spawnerName, "W22Spawner"))
						orxObject_AddFX       (object_spawned, "W22FX");
					break;
				}
			}
			break;
		}

		case orxEVENT_TYPE_FX:
		{
			orxFX_EVENT_PAYLOAD *fx_info = (orxFX_EVENT_PAYLOAD *)_pstEvent->pstPayload;

			if (!orxString_Compare(fx_info->zFXName, "W21FX"))
			{
				switch(_pstEvent->eID)
				{
					case orxFX_EVENT_START:
					{
						orxObject_SetVolume(stage, 0.0f);
						break;
					}
				}
			}
			else if (!orxString_Compare(fx_info->zFXName, "W22FX"))
			{
				switch(_pstEvent->eID)
				{
					case orxFX_EVENT_START:
					{
						int index;
						orxOBJECT *boss = (orxOBJECT*)_pstEvent->hRecipient;
						orxCOLOR visible;

						orxObject_GetColor(boss_name_label, &visible);
						visible.fAlpha = 1.0f;
						orxObject_SetColor(boss_name_label, &visible);

						orxOBJECT *pattern = orxObject_GetChild(bossPatterns[0] = orxObject_GetChild(boss));
						for (index = 0; index < 4; index++)
						{
							orxObject_GetColor(bossHealthBars[index], &visible);
							visible.fAlpha = 1.0f;
							orxObject_SetColor(bossHealthBars[index], &visible);

							do
								orxSpawner_Enable(orxOBJECT_GET_STRUCTURE(pattern, SPAWNER), orxFALSE);
							while ((pattern = orxObject_GetSibling(pattern)) != orxNULL);

							if (index < 3)
								pattern = orxObject_GetChild(bossPatterns[index+1] = orxObject_GetSibling(bossPatterns[index]));
						}
						break;
					}
					case orxFX_EVENT_STOP:
					{
						enableBossPattern(0, orxTRUE);
						break;
					}
				}
			}
			break;
		}
		default:
			break;
	}
	return orxSTATUS_SUCCESS;
}

orxSTATUS orxFASTCALL StageTwoHandler(const orxEVENT *_pstEvent)
{
	orxFX_EVENT_PAYLOAD *fx_info;
	static orxOBJECT *dummy;
	static orxOBJECT *dummy2;
	static orxOBJECT *dummyHolder;
	orxSPAWNER *enemySpawner;
	//int array[] = {1,2,3};

	switch(_pstEvent->eType)
	{
		case orxEVENT_TYPE_SPAWNER:
		{
			orxSPAWNER* spawner = (orxSPAWNER*)_pstEvent->hSender;
			orxOBJECT*  object_spawned = (orxOBJECT*)_pstEvent->hRecipient;
			orxVECTOR position;
			//orxVECTOR player_position;

			switch(_pstEvent->eID)
			{
				case orxSPAWNER_EVENT_SPAWN:
					enemySpawner = orxOBJECT_GET_STRUCTURE(object_spawned, SPAWNER);
					//orxVECTOR objectPosition;

					if(enemySpawner != orxNULL)
						orxLOG("THE ESPAWNER IS %s", orxSpawner_GetName(enemySpawner));

					if(!orxString_Compare(orxSpawner_GetName(spawner), "FireAtPlayer") ||
					   !orxString_Compare(orxSpawner_GetName(spawner), "W8-9FireAtPlayer"))
					{
						orxVECTOR newForce,
								  bulletSpeed,
								  target;

						orxSpawner_SetWaveDelay(spawner, 2.0);
						orxObject_GetPosition(player, &target);
						orxObject_GetPosition(object_spawned, &position);

						bulletSpeed.fX = 128;
						bulletSpeed.fY = 128;
						bulletSpeed.fZ = 0;

						orxVector_Sub( &newForce, &target, &position );
						orxVector_Normalize( &newForce, &newForce);
						orxVector_Mul(&bulletSpeed, &newForce, &bulletSpeed);
						orxObject_SetSpeed(object_spawned, &bulletSpeed);
					}
					if(!orxString_Compare(orxSpawner_GetName(spawner), "W1Spawner"))
					{
						orxSpawner_Enable(enemySpawner, orxFALSE);
						orxObject_AddFX(object_spawned, "W1FX_ENTER");
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W2Spawner"))
					{
						orxSpawner_Enable(enemySpawner, orxFALSE);
						orxObject_AddFX(object_spawned, "W2FX_ENTER");
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W3Spawner"))
					{
						orxObject_AddFX(object_spawned, "W3FX_ENTER");
						 orxSpawner_SetWaveDelay(enemySpawner, 1.0f);
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W4Spawner"))
					{
						orxObject_AddFX(object_spawned, "W4FX_ENTER");
						orxSpawner_SetWaveDelay(enemySpawner, 1.0f);
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W5Spawner"))
					{
						orxVECTOR pos;
						orxSpawner_SetWaveDelay(spawner, 0.0f);
						orxObject_GetPosition(object_spawned, &pos);
						orxSPAWNER *dummySpawner;

						dummy = orxObject_GetChild(object_spawned);
						dummyHolder = object_spawned;
						dummySpawner = orxOBJECT_GET_STRUCTURE(dummy, SPAWNER);
						orxSpawner_Enable(dummySpawner, orxFALSE);

						orxObject_AddFX(object_spawned, "W5FX_ENTER");
						orxSpawner_Enable(enemySpawner, orxFALSE);
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W5DummySpawner"))
					{
						orxVECTOR pos, dup;
						pos.fZ = 0.0f;
						orxSpawner_Enable(enemySpawner, orxFALSE);

						switch(orxSpawner_GetTotalObjectCounter(spawner))
						{
							case 1:
								orxObject_SetParent(object_spawned, dummy);

								pos.fY =  0.0;
								pos.fX = 64.0f;
								orxObject_SetPosition(object_spawned, &pos);
								orxSpawner_Enable(enemySpawner, orxTRUE);
								orxObject_GetWorldPosition(object_spawned, &dup);
								break;
							case 2:
								orxObject_SetParent(object_spawned, dummy);

								pos.fY = 0.0;
								pos.fX = - 64.0f;
								orxObject_SetPosition(object_spawned, &pos);
								orxSpawner_Enable(enemySpawner, orxTRUE);
								orxObject_GetWorldPosition(object_spawned, &dup);
								break;
							case 3:
								orxObject_SetParent(object_spawned, dummy);

								pos.fX = 0.0 ;
								pos.fY = 64.0f;
								orxObject_SetPosition(object_spawned, &pos);
								orxSpawner_Enable(enemySpawner, orxTRUE);
								orxObject_GetWorldPosition(object_spawned, &dup);
								break;
							case 4:
								orxObject_SetParent(object_spawned, dummy);

								pos.fX = 0.0;
								pos.fY = -64.0f;
								orxObject_SetPosition(object_spawned, &pos);
								orxObject_AddFX(dummy, "ROTATE");
								orxObject_AddFX(dummyHolder, "W5FX_MID");
								orxSpawner_Enable(enemySpawner, orxTRUE);
								orxObject_GetWorldPosition(object_spawned, &dup);
								break;
						}
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "FireCircle"))
					{
						orxSpawner_SetWaveDelay(spawner, 1.5);
						orxSpawner_SetRotation(spawner, (orxSpawner_GetTotalObjectCounter(spawner) * orxMATH_KF_PI) / 5);
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W6Spawner"))
					{
						orxVECTOR objectPosition;
						orxObject_GetPosition(object_spawned, &objectPosition);

						orxSpawner_Enable(enemySpawner, orxFALSE);

						switch(orxSpawner_GetTotalObjectCounter(spawner) % 4)
						{
							case 0:
								objectPosition.fX -= 384.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
							case 1:
								break;
							case 2:
								objectPosition.fX -= 128.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
							case 3:
								objectPosition.fX -= 256.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
						}
						orxObject_AddFX(object_spawned, "W6FX_ENTER");
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W7Spawner"))
					{
						orxVECTOR objectPosition;
						orxObject_GetPosition(object_spawned, &objectPosition);

						orxSpawner_Enable(enemySpawner, orxFALSE);

						switch(orxSpawner_GetTotalObjectCounter(spawner) % 3)
						{
							case 0:
								objectPosition.fX += 256.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
							case 1:
								break;
							case 2:
								objectPosition.fX += 128.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
						}
						orxObject_AddFX(object_spawned, "W7FX_ENTER");
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W8Spawner"))
					{
						orxVECTOR pos;
						orxObject_GetWorldPosition(object_spawned , &pos);

						dummy = object_spawned;
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W9Spawner"))
					{
						orxVECTOR pos;
						orxObject_GetWorldPosition(object_spawned , &pos);

						dummy2 = object_spawned;
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W8DummySpawner"))
					{
						orxVECTOR objectPosition;

						switch(orxSpawner_GetTotalObjectCounter(spawner))
						{
							case 1:
								orxObject_SetParent(object_spawned, dummy);
								objectPosition.fZ = objectPosition.fY = 0;
								objectPosition.fX = -128.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
							case 2:
								orxObject_SetParent(object_spawned, dummy);
								objectPosition.fZ = objectPosition.fY = 0;
								objectPosition.fX = 128.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
							case 3:
								orxObject_SetParent(object_spawned, dummy);
								objectPosition.fZ = objectPosition.fX = 0;
								objectPosition.fY = -128.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
							case 4:
								orxObject_SetParent(object_spawned, dummy);
								objectPosition.fZ = objectPosition.fX = 0;
								objectPosition.fY = 128.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
							case 5:
								orxObject_SetParent(object_spawned, dummy);
								objectPosition.fZ = 0;
								objectPosition.fY = -64.0f;
								objectPosition.fX = -64.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
							case 6:
								orxObject_SetParent(object_spawned, dummy);
								objectPosition.fZ = 0;
								objectPosition.fY = 64.0f;
								objectPosition.fX = -64.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
							case 7:
								orxObject_SetParent(object_spawned, dummy);
								objectPosition.fZ = 0;
								objectPosition.fY = 64.0f;
								objectPosition.fX = 64.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
							case 8:
								orxObject_SetParent(object_spawned, dummy);
								objectPosition.fZ = 0;
								objectPosition.fY = -64.0f;
								objectPosition.fX = 64.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								orxObject_AddFX(dummy, "ROTATE");
								orxObject_AddFX(dummy, "W8FX_ENTER");
								break;
						}
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W9DummySpawner"))
					{
						orxVECTOR objectPosition;

						switch(orxSpawner_GetTotalObjectCounter(spawner))
						{
							case 1:
								orxObject_SetParent(object_spawned, dummy2);
								objectPosition.fZ = 72;
								objectPosition.fY = 0;
								objectPosition.fX = -128.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
							case 2:
								orxObject_SetParent(object_spawned, dummy2);
								objectPosition.fZ = 72;
								objectPosition.fY = 0;
								objectPosition.fX = 128.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
							case 3:
								orxObject_SetParent(object_spawned, dummy2);
								objectPosition.fZ = 72 ;
								objectPosition.fX = 0;
								objectPosition.fY = -128.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
							case 4:
								orxObject_SetParent(object_spawned, dummy2);
								objectPosition.fZ = 72 ;
								objectPosition.fX = 0;
								objectPosition.fY = 128.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
							case 5:
								orxObject_SetParent(object_spawned, dummy2);
								objectPosition.fZ = 72;
								objectPosition.fY = -64.0f;
								objectPosition.fX = -64.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
							case 6:
								orxObject_SetParent(object_spawned, dummy2);
								objectPosition.fZ = 72;
								objectPosition.fY = 64.0f;
								objectPosition.fX = -64.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
							case 7:
								orxObject_SetParent(object_spawned, dummy2);
								objectPosition.fZ = 72;
								objectPosition.fY = 64.0f;
								objectPosition.fX = 64.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
							case 8:
								orxObject_SetParent(object_spawned, dummy2);
								objectPosition.fZ = 72;
								objectPosition.fY = -64.0f;
								objectPosition.fX = 64.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								orxObject_AddFX(dummy2, "CWROTATE");
								orxObject_AddFX(dummy2, "W9FX_ENTER");
								break;
						}
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W10Spawner"))
						dummy = object_spawned;
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W10DummySpawner"))
					{
						orxVECTOR object_position;
						orxSpawner_Enable(enemySpawner, orxTRUE);

						orxSpawner_SetWaveDelay(enemySpawner, 1.0f);
						switch(orxSpawner_GetTotalObjectCounter(spawner))
						{
							case 1:
								orxObject_SetParent(object_spawned, dummy);
								object_position.fX = 0;
								object_position.fZ = 72;
								object_position.fY = 0;
								orxObject_SetPosition(object_spawned, &object_position);
								break;
							case 2:
								orxObject_SetParent(object_spawned, dummy);
								object_position.fX = 0;
								object_position.fZ = 72;
								object_position.fY = 100;
								orxObject_SetPosition(object_spawned, &object_position);
								break;
							case 3:
								orxObject_SetParent(object_spawned, dummy);
								object_position.fX = 0;
								object_position.fZ = 72;
								object_position.fY = -100;
								orxObject_SetPosition(object_spawned, &object_position);
								orxObject_AddFX(dummy, "W10FX_ENTER");
								break;
						}
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W11Spawner"))
						dummy2 = object_spawned;
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W11DummySpawner"))
					{
						orxVECTOR object_position;
						object_position.fX = -200;
						object_position.fZ = 72;
						object_position.fY = 0;
						orxSpawner_SetObjectSpeed (enemySpawner, &object_position);
						orxSpawner_Enable(enemySpawner, orxTRUE);
						orxSpawner_SetWaveDelay(enemySpawner, 1.0f);

						switch(orxSpawner_GetTotalObjectCounter(spawner))
						{
							case 1:
								orxObject_SetParent(object_spawned, dummy2);
								object_position.fX = 0;
								object_position.fZ = 72;
								object_position.fY = 0;
								orxObject_SetPosition(object_spawned, &object_position);
								break;
							case 2:
								orxObject_SetParent(object_spawned, dummy2);
								object_position.fX = 0;
								object_position.fZ = 72;
								object_position.fY = 100;
								orxObject_SetPosition(object_spawned, &object_position);
								break;
							case 3:
								orxObject_SetParent(object_spawned, dummy2);
								object_position.fX = 0;
								object_position.fZ = 72;
								object_position.fY = -100;
								orxObject_SetPosition(object_spawned, &object_position);
								orxObject_AddFX(dummy2, "W11FX_ENTER");
								break;
						}
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W10-11FireSapwner"))
						orxSpawner_SetWaveDelay(spawner, 0.25f);
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W12Spawner"))
					{
						orxObject_AddFX(object_spawned, "W12FX_ENTER");
						orxObject_AddFX(object_spawned, "W12FX_ALONG");
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "FireCircleW12"))
					{
						orxSpawner_SetWaveDelay(spawner, 2.0);
						orxSpawner_SetRotation(spawner, ((orxSpawner_GetTotalObjectCounter(spawner) * orxMATH_KF_PI) / 5.0f) + (orxMATH_KF_PI / 5.0f ));
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W13Spawner"))
					{
						orxObject_AddFX(object_spawned, "W13FX_ENTER");
						orxObject_AddFX(object_spawned, "W13FX_ALONG");
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W14Spawner"))
					{
						orxVECTOR objectPosition;
						orxObject_GetPosition(object_spawned, &objectPosition);
						orxSpawner_Enable(enemySpawner, orxFALSE);
						switch(orxSpawner_GetTotalObjectCounter(spawner) % 3)
						{
							case 0:
								objectPosition.fX += 200.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
							case 1:
								objectPosition.fY -= 100.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
							case 2:
								objectPosition.fX -= 200.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
						}
						orxObject_AddFX(object_spawned, "W14FX_ENTER");
						orxObject_AddFX(object_spawned, "W14FX_EXIT");
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "FireW14"))
					{
						orxVECTOR newForce,
						  bulletSpeed,
						  target;

						orxVECTOR objectPosition;
						orxObject_GetPosition(object_spawned, &objectPosition);

						switch(orxSpawner_GetTotalObjectCounter(spawner) % 3)
						{
							case 0:
								objectPosition.fX += 30.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
							case 1:
								break;
							case 2:
								objectPosition.fX -= 30.0f;
								orxObject_SetPosition(object_spawned, &objectPosition);
								break;
						}

						orxObject_GetPosition(player, &target);
						orxObject_GetPosition(object_spawned, &position);

						bulletSpeed.fX = 200.0f;
						bulletSpeed.fY = 200.0f;
						bulletSpeed.fZ = 0.0f;

						orxVector_Sub( &newForce, &target, &position );
						orxVector_Normalize( &newForce, &newForce);
						orxVector_Mul(&bulletSpeed, &newForce, &bulletSpeed);
						orxObject_SetSpeed(object_spawned, &bulletSpeed);
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W15Spawner"))
					{
						orxObject_AddFX(object_spawned, "W15FX_ENTER");
						orxObject_AddFX(object_spawned, "W15FX_ALONG");
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "FireW15"))
					{
						orxSpawner_SetWaveDelay(spawner, 2.0);
						orxSpawner_SetRotation(spawner, (orxSpawner_GetTotalObjectCounter(spawner) * orxMATH_KF_PI) / 5.0f);
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W16Spawner"))
					{
						orxObject_AddFX(object_spawned, "W16FX_ENTER");
						orxObject_AddFX(object_spawned, "W16FX_ALONG");
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W17Spawner"))
					{
						orxSpawner_Enable(enemySpawner, orxFALSE);
						orxObject_AddFX(object_spawned, "W17FX_ENTER");
					}
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W18Spawner"))
						orxObject_AddFX(object_spawned, "W18FX_ENTER");
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W19Spawner"))
						orxObject_AddFX(object_spawned, "W19FX_ENTER");
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "FireW17"))
						orxSpawner_SetRotation(spawner, (((orxSpawner_GetTotalObjectCounter(spawner) % 20 ) * (orxMATH_KF_PI /  20.0f))) - (orxMATH_KF_PI / 2.0f));
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "FireW18"))
						orxSpawner_SetRotation(spawner, (((orxSpawner_GetTotalObjectCounter(spawner) % 3 ) * (orxMATH_KF_PI / 3.0f))) - (orxMATH_KF_PI));
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "FireW19"))
						orxSpawner_SetRotation(spawner, (((orxSpawner_GetTotalObjectCounter(spawner) % 3 ) * (orxMATH_KF_PI / 3.0f))));
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W20Spawner"))
						orxObject_AddFX(object_spawned, "W20FX_ENTER");
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W21Spawner"))
						orxObject_AddFX(object_spawned, "W21FX_ENTER");
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "FireW21"))
						orxSpawner_SetRotation(spawner, (orxSpawner_GetTotalObjectCounter(spawner) * orxMATH_KF_PI) / 5.0f);
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "FireW22"))
						orxSpawner_SetRotation(spawner, (tan(orxSpawner_GetTotalObjectCounter(spawner)) * orxMATH_KF_PI) / 5.0f);
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "FireW23-1"))
						orxSpawner_SetRotation(spawner,
							(((orxSpawner_GetTotalObjectCounter(spawner) % 2) * orxMATH_KF_PI))
							+ (((orxSpawner_GetTotalObjectCounter(spawner) - 1) / 2) * .4));
//					else if(!orxString_Compare(orxSpawner_GetName(spawner), "W21-2Spawner"))
//						orxObject_AddFX(object_spawned, "W21FX_ENTER");
					else if(!orxString_Compare(orxSpawner_GetName(spawner), "FireW23-2"))
					{
						orxVECTOR newForce,
								  bulletSpeed,
								  target;

						orxSpawner_SetWaveDelay(spawner, 2.0);
						orxObject_GetPosition(player, &target);
						orxObject_GetPosition(object_spawned, &position);

						bulletSpeed.fX = 128;
						bulletSpeed.fY = 128;
						bulletSpeed.fZ = 0;

						orxVector_Sub( &newForce, &target, &position );
						orxVector_Normalize( &newForce, &newForce);
						orxVector_Mul(&bulletSpeed, &newForce, &bulletSpeed);
						orxObject_SetSpeed(object_spawned, &bulletSpeed);
					}
				break;
			}
			break;
		}

		case orxEVENT_TYPE_FX:
		{
			fx_info = (orxFX_EVENT_PAYLOAD *)_pstEvent->pstPayload;
			orxOBJECT *object    = (orxOBJECT *)_pstEvent->hRecipient;
			orxSPAWNER *enemySpawner;

			switch(_pstEvent->eID)
			{
				case orxFX_EVENT_STOP:
				{
					if(!orxString_NCompare(fx_info->zFXName , "W", 1))
					{
						enemySpawner = orxOBJECT_GET_STRUCTURE(object, SPAWNER);

						if(!orxString_Compare(fx_info->zFXName, "W1FX_ENTER"))
						{
							orxObject_AddFX(object, "W1FX_EXIT");
							orxSpawner_Enable(enemySpawner, orxTRUE);
						}
						else if(!orxString_Compare(fx_info->zFXName, "W2FX_ENTER"))
						{
							orxObject_AddFX(object, "W2FX_EXIT");
							orxSpawner_Enable(enemySpawner, orxTRUE);
						}
						else if(!orxString_Compare(fx_info->zFXName, "W3FX_ENTER"))
						{
							orxObject_AddFX(object, "W3FX_EXIT");
							 orxSpawner_Reset(enemySpawner);
							orxSpawner_Enable(enemySpawner, orxTRUE);
							orxSpawner_SetWaveDelay(enemySpawner, 0.5f);
						}
						else if(!orxString_Compare(fx_info->zFXName, "W4FX_ENTER"))
						{
							orxObject_AddFX(object, "W4FX_EXIT");
							orxSpawner_Reset(enemySpawner);
							orxSpawner_Enable(enemySpawner, orxTRUE);
							orxSpawner_SetWaveDelay(enemySpawner, 0.5f);
						}
						else if(!orxString_Compare(fx_info->zFXName, "W5FX_ENTER"))
						{
							orxSPAWNER *dummySpawner;
							dummySpawner = orxOBJECT_GET_STRUCTURE(dummy, SPAWNER);
							orxSpawner_Enable(dummySpawner, orxTRUE);
						}
						else if(!orxString_Compare(fx_info->zFXName, "W6FX_ENTER"))
						{
							orxObject_AddFX(object, "W6FX_EXIT");
							orxSpawner_Enable(enemySpawner, orxTRUE);
						}
						else if(!orxString_Compare(fx_info->zFXName, "W7FX_ENTER"))
						{
							orxObject_AddFX(object, "W7FX_EXIT");
							orxSpawner_Enable(enemySpawner, orxTRUE);
						}
						else if(!orxString_Compare(fx_info->zFXName, "W8FX_ENTER"))
							orxObject_AddFX(object, "W8FX_EXIT");
						else if(!orxString_Compare(fx_info->zFXName, "W9FX_ENTER"))
							orxObject_AddFX(object, "W9FX_EXIT");
						else if(!orxString_Compare(fx_info->zFXName, "W10FX_ENTER"))
						{
							orxObject_AddFX(dummy, "W10and11FX_EXIT");
							orxObject_AddFX(dummy2, "W10and11FX_EXIT");
						}
						else if(!orxString_Compare(fx_info->zFXName, "W14FX_ENTER"))
							orxSpawner_Enable(enemySpawner, orxTRUE);
						else if(!orxString_Compare(fx_info->zFXName, "W17FX_ENTER"))
						{
							orxSpawner_Enable(enemySpawner, orxTRUE);
							orxObject_AddFX(object, "W17FX_EXIT");
						}
						else if (!orxString_Compare(fx_info->zFXName, "W20FX_ENTER"))
							orxObject_AddFX(object, "W20FX_EXIT");
						else if (!orxString_Compare(fx_info->zFXName, "W21FX_ENTER"))
						{
							enableBossPattern(0, orxTRUE);
						}
					}
					break;
				}
				case  orxFX_EVENT_START:
					if (!orxString_Compare(fx_info->zFXName, "W20FX_ENTER"))
						orxObject_SetVolume(stage, 0.0f);
					else if (!orxString_Compare(fx_info->zFXName, "W21FX_ENTER"))
					{
						int index;
						orxOBJECT *boss = (orxOBJECT*)_pstEvent->hRecipient;
						orxCOLOR visible;

						orxOBJECT *pattern = orxObject_GetChild(bossPatterns[0] = orxObject_GetChild(boss));
						for (index = 0; index < 4; index++)
						{
							orxObject_GetColor(bossHealthBars[index], &visible);
							visible.fAlpha = 1.0f;
							orxObject_SetColor(bossHealthBars[index], &visible);

							do
								orxSpawner_Enable(orxOBJECT_GET_STRUCTURE(pattern, SPAWNER), orxFALSE);
							while ((pattern = orxObject_GetSibling(pattern)) != orxNULL);

							if (index < 3)
								pattern = orxObject_GetChild(bossPatterns[index+1] = orxObject_GetSibling(bossPatterns[index]));
						}
					}

					break;
			}
			break;
		}
		default:
			break;
	}
	return orxSTATUS_SUCCESS;
}

/* The game's main loop. Most of the logic will be handled here. */
void orxFASTCALL Update(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	orxFLOAT move_x = 0.f, move_y = 0.f;
	orxVECTOR speed;
	orxBOOL disable = 0;

	if (!orxString_Compare("UserGameControls", orxInput_GetCurrentSet()))
	{
		/* Check for player input. */
		if (orxInput_IsActive("Focus"))
		{
			if (orxInput_IsActive("Up"))
				move_y -= 65.f;
			if (orxInput_IsActive("Down"))
				move_y += 65.f;
			if (orxInput_IsActive("Left"))
				move_x -= 65.f;
			if (orxInput_IsActive("Right"))
				move_x += 65.f;
		}
		else
		{
			if (orxInput_IsActive("Up"))
				move_y -= 160.f;
			if (orxInput_IsActive("Down"))
				move_y += 160.f;
			if (orxInput_IsActive("Left"))
				move_x -= 160.f;
			if (orxInput_IsActive("Right"))
				move_x += 160.f;
		}
		orxSpawner_Enable(main_weapon, orxInput_IsActive("Shoot"));

		if (orxInput_IsActive("Bomb") && orxInput_HasNewStatus("Bomb") && (--num_bombs) >= 0)
		{
			char BombsString[4];
			sprintf(BombsString, "%d", (int)num_bombs);
			orxObject_SetTextString(bombs_label, BombsString);

			/* disable spawner */
			orxSpawner_Enable(main_weapon, disable);

			/*Erase all bullets
			Bombing();*/

			/* Erase all Enemies */
			deleteEnemies(orxTRUE);
			deleteBullets();

			/* Flash the sceen with when boming */
			orxObject_AddFX(BombingFlash, "BombingFlashFX");

			/*orxObject_SetColor(BombingFlash, &FlashColor);*/
			/* Set back ground  to withe
			orxViewport_SetBackgroundColor(viewport, );
			*/
			/* Shake viewport */
			orxObject_AddFX(hud_pointer, "ScreenShakeFX");
			orxObject_AddFX(camera_parent, "ScreenShakeFX");
			orxObject_AddFX(BombingFlash, "ScreenShakeFX");

			/*
			orxViewport_SetPosition(viewport,)
			*/
		}
		orxVector_Set(&speed, move_x, move_y, 0);
		orxObject_SetSpeed(player, &speed);
	}
	else
	{
		if (orxInput_IsActive("SkipIntro"))
			LoadScene(1);

		if (orxInput_IsActive("StartGame"))
			LoadScene(2);

		if (orxInput_IsActive("MenuUp") && orxInput_HasNewStatus("MenuUp"))
			HighlightMenuOption(option_index - 1);

		if (orxInput_IsActive("MenuDown") && orxInput_HasNewStatus("MenuDown"))
			HighlightMenuOption(option_index + 1);

		if (orxInput_IsActive("MenuLeft") && orxInput_HasNewStatus("MenuLeft"))
			HighlightMenuOptionEntry(orxFALSE);

		if (orxInput_IsActive("MenuRight") && orxInput_HasNewStatus("MenuRight"))
			HighlightMenuOptionEntry(orxTRUE);

		if (orxInput_IsActive("MenuSelect") && orxInput_HasNewStatus("MenuSelect"))
			HandleMenuOption(orxFALSE);

		if (orxInput_IsActive("MenuBack") && orxInput_HasNewStatus("MenuBack"))
			HandleMenuOption(orxTRUE);

		if (orxInput_IsActive("SkipOuttro") && orxInput_HasNewStatus("SkipOuttro"))
			orxEvent_SendShort(orxEVENT_TYPE_SYSTEM, orxSYSTEM_EVENT_CLOSE);
	}
}

static void orxFASTCALL deleteEnemies(orxBOOL damage)
{
	orxENEMY *prevNode  = orxNULL;
	orxENEMY *enemyNode = (orxENEMY *)orxLinkList_GetFirst(&enemyList);

	if(damage)
	{
		while (enemyNode != orxNULL)
		{
			orxConfig_PushSection(orxObject_GetName(enemyNode->enemy));
			if ((enemyNode->health -= 50)/*orxConfig_GetS32("Strength")*/ <= 0)
			{
				if ((--enemyNode->bars) <= 0)
				{
					if (orxConfig_GetS32("Bars") > 1)
					{ // Boss Death
						orxCOLOR invisible;
						orxObject_GetColor(bossHealthBars[0], &invisible);
						invisible.fAlpha = 0.0f;
						orxObject_SetColor(bossHealthBars[0], &invisible);

						deleteBullets();

						LoadScene(6);
					}

					orxLinkList_Remove((orxLINKLIST_NODE *)enemyNode);

					orxLinkList_Clean(enemyNode->shotlist);

				orxObject_SetLifeTime(enemyNode->enemy, orxFLOAT_0);
				}
				else
				{ // Boss Pattern Change
					orxCOLOR invisible;
					orxObject_GetColor(bossHealthBars[0], &invisible);
					invisible.fAlpha = 0.0f;

					enemyNode->health = orxConfig_GetS32("Health");

					enableBossPattern(4 - enemyNode->bars - 1, orxFALSE);

					if (enemyNode->bars > 0)
						enableBossPattern(4 - enemyNode->bars, orxTRUE);

					orxObject_SetColor(bossHealthBars[enemyNode->bars], &invisible);

					deleteBullets();
				}
			}
			else
			{
				if (orxConfig_GetS32("Bars") == 4)
				{
					scale.fX = (enemyNode->health / (orxFLOAT)orxConfig_GetS32("Health")) * 512;
					orxObject_SetScale(bossHealthBars[enemyNode->bars - 1], &scale);
				}
			}
			orxConfig_PopSection();

			if (prevNode != orxNULL && orxLinkList_GetNext((orxLINKLIST_NODE *)prevNode) == (orxLINKLIST_NODE *)enemyNode)
			{
				prevNode  = enemyNode;
				enemyNode = (orxENEMY *)orxLinkList_GetNext((orxLINKLIST_NODE *)enemyNode);
			}
			else
			{
				if (prevNode == orxNULL)
				{
					if (enemyNode == (orxENEMY *)orxLinkList_GetFirst(&enemyList))
					{
						prevNode  = enemyNode;
						enemyNode = (orxENEMY *)orxLinkList_GetNext((orxLINKLIST_NODE *)enemyNode);
					}
					else
						enemyNode = (orxENEMY *)orxLinkList_GetFirst(&enemyList);
				}
				else
					enemyNode = (orxENEMY *)orxLinkList_GetNext((orxLINKLIST_NODE *)prevNode);
			}
		}
	}
	else
	{
		while(enemyNode = (orxENEMY *)orxLinkList_GetFirst(&enemyList), enemyNode != orxNULL)
			if(orxLinkList_Remove((orxLINKLIST_NODE *)enemyNode) == orxSTATUS_SUCCESS )
				orxObject_SetLifeTime(enemyNode->enemy, orxFLOAT_0);
	}
	return;
}

static void orxFASTCALL deleteBullets()
{
	orxBULLET *bulletNode;

	while(bulletNode = (orxBULLET *)orxLinkList_GetFirst(&bulletList), bulletNode != orxNULL)
		if(orxLinkList_Remove((orxLINKLIST_NODE *)bulletNode) == orxSTATUS_SUCCESS )
			orxObject_SetLifeTime(bulletNode->bullet, orxFLOAT_0);

	return;
}

orxSTATUS orxFASTCALL Init()
{
	orxS32 inner,
           outer,
           zouter;


	memBankLinkedList = orxBank_Create(256, sizeof(orxLINKLIST),  orxBANK_KU32_FLAG_NONE, orxMEMORY_TYPE_MAIN);
	memBankEnemy      = orxBank_Create(256, sizeof(orxENEMY),     orxBANK_KU32_FLAG_NONE, orxMEMORY_TYPE_MAIN);
	memBankShot       = orxBank_Create(256, sizeof(orxSHOT),      orxBANK_KU32_FLAG_NONE, orxMEMORY_TYPE_MAIN);
	memBankBullet     = orxBank_Create(512, sizeof(orxBULLET),    orxBANK_KU32_FLAG_NONE, orxMEMORY_TYPE_MAIN);

	/* Initialize menus array */
	for (inner = 0; inner < NUM_MENUS; inner++)
		for(outer = 0; outer < MAX_MENU_OPTIONS; outer++)
			for(zouter = 0; zouter < MAX_MENU_OPTIONS; zouter++)
				menus[inner][outer][zouter] = orxNULL;

	/* Registers our update callback */
	orxClock_Register(orxClock_FindFirst(orx2F(-1.0f), orxCLOCK_TYPE_CORE),
			Update, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);

	/* Registers handlers for events we must listen for.*/
	orxEvent_AddHandler(orxEVENT_TYPE_PHYSICS, EventHandler);
	orxEvent_AddHandler(orxEVENT_TYPE_FX, EventHandler);
	orxEvent_AddHandler(orxEVENT_TYPE_INPUT, EventHandler);
	orxEvent_AddHandler(orxEVENT_TYPE_SPAWNER, EventHandler);
	orxEvent_AddHandler(orxEVENT_TYPE_CLOCK, EventHandler);

	/* Register Inputs */
	orxInput_Load(orxSTRING_EMPTY);

	/* Load the intro scene */
	LoadScene(0);

	/* Done! */
	return orxSTATUS_SUCCESS;
}

orxSTATUS orxFASTCALL Run()
{
	orxSTATUS result = orxSTATUS_SUCCESS;

	/* Should quit? */
	if (orxInput_IsActive("Quit") || orxInput_IsActive("Quit2"))
		result = orxSTATUS_FAILURE;

	/* Done! */
	return result;
}

void orxFASTCALL Exit() { }

int main(int argc, char **argv)
{
	orx_Execute(argc, argv, Init, Run, Exit);

	return EXIT_SUCCESS;
}
