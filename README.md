# NetWar
## How to run
For Windows users, just open the bin folder and run Leviathan.exe.

For Linux and Mac users, the source code should build without modifications, although it has not been tested in Mac. Eclipse project files are included. In the future, I may include project files for codelite.

## History
Although my team and I have worked on smaller, individual programs, NetWar is the first project we had ever worked on. I and two of my classmates completed the program as our senior project. Due to the time constraints, many of the things we had hoped to implement were not realized in our game. We had a little under 8 months to complete the entire game, and we consider the project an overall success. As part of our graduation requirements, we had to present the program in the annual Software Engineering Expo at Pensacola Christian College, and the feedback for the game was overwhelmingly positive, even from people that don't normally play video games.

## Development
Each stage was developed by a member of the team (I had the pleasure of creating the first stage). The game was coded using the ORX portable game engine, and was written using C. It was supposed to have original graphics and look very futuristic, as if the player were fighting inside a computer. However, the graphic artist was overwhelmed with school projects, and thus dropped his support. We instead opted to use [graphics from the Tyrian project][1] and attempt to make them fit the theme, taking computer terms (like firewall) and make literal graphical representations of those terms. Early on, we chose to use git, and chose to use Eclipse for an IDE. However, Eclipse's user interface caused us many headaches and the git interoperability left much to be desired, and due to some bugs even almost destroyed our project. We dumped Eclipse and git with it, as the team didn't have time to learn the console interface for it so close to the end of the project.

### Tools
- Eclipse CDT
- MinGW 32 / GCC 4.1
- ORX Portable Game Engine
- Winmerge
- GDB

## Status
The game is not 100% perfect, and still has some work that would need to be done before it is even considered for a "commercial" release, but it is, in its current form, what we displayed to the student body of Pensacola Christian College.

## Gameplay
The player may customize the number of lives and bombs he starts with, and must make it through the level and delete the boss virus. After defeating an enemy, the player gains points based on that enemy's max HP. The player gets no points if he uses a bomb to destroy enemies (so don't kill the boss with a bomb!). The only part of the ship that is vulnerable is the center; bullets that pass through anything else will do the player no harm. There are 3 types of ships, each with their own special weapon: one that shoots homing missiles; one that shoots in a wide spread, and one that shoots in a narrow column like a laser. Each has their advantages and disadvantages.

## Credits
- Peter K -  Team Manager, Stage 3 designer
- Charles Christie (me) Stage 1 designer, git repository administrator
- Javier C. - Stage 2 designer
- Kim S. - Logo Designer
- Peter R. - Musician, Software Engineering Incorporated Project Manager, Assistant Programmer
- [Daniel Cook - Tyrian Remastered Graphics][1]

[1]: http://www.lostgarden.com/2007/04/free-game-graphics-tyrian-ships-and.html